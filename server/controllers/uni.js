//Подключаем обработчик ошибок
const errorHandler = require('../utils/errorHandler');

//Подключаем базу данных
const db = require('../db/index');

//Контроллер для выборки смартфильтров
module.exports.getSmartFilters = async function(request, response) {
    try{
        await db.queryRows("select * from uni.get_smart_filters($1) order by id_parent, sortorder",
        [
            request.body.keyUser
        ], (error, result) => {
            if (error) {
                errorHandler(response, error)
            } else {
                response.status(200).json(result)
            }
        })
    } catch (error) {
        errorHandler(response, error)
    }
};
