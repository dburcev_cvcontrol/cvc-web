//Подключаем базу данных
const db = require('../db/db.controllers');

module.exports = {
    getPays: async (request, response) => {
        await db.controllerQuery(request, response,
            `select *,
            (select array_to_json(array_agg(r.*)) from licenses.pay_rows as r where r.id_pay = p.id_pay) as rows 
            from licenses.pay as p where p.id_client = $1
            order by p.sort_order`,
            [request.params.id]);
    },

    getPay: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `select *,
                (select array_to_json(array_agg(r.*)) from licenses.pay_rows as r where r.id_pay = p.id_pay) as rows
                from licenses.pay as p where p.id_pay = $1`,
            [request.params.id]);
    },

    addPay: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `insert into licenses.pay values ($1, $2, $3, $4, $5, $6, $7, $8)`,
            [request.body.id_pay,
                request.body.id_client,
                request.body.plan_number,
                request.body.plan_date,
                request.body.fact_number,
                request.body.fact_date,
                request.body.comment,
                request.body.sum
            ]);
        if (request.body.rows) {
            request.body.rows.forEach(row => {
                if (!row.deleted) {
                    db.controllerQueryOneRow(request, response,
                        `insert into licenses.pay_rows values ($1, $2, $3, $4, $5)`,
                        [row.id_pay_row,
                            row.id_pay,
                            row.name,
                            row.serial_key,
                            row.sum
                        ]);
                }
            })
        }
    },

    updatePay: async (request, response) => {
        console.log(request.body);
        await db.controllerQueryOneRow(request, response,
            `update licenses.pay set
                    plan_number = $1, 
                    plan_date = $2, 
                    fact_number = $3, 
                    fact_date = $4, 
                    comment = $5, 
                    sum = $6
                 where id_pay = $7`,
            [
                request.body.plan_number,
                request.body.plan_date,
                request.body.fact_number,
                request.body.fact_date,
                request.body.comment,
                request.body.sum,
                request.body.id_pay
            ]);
        if (request.body.rows) {
            request.body.rows.forEach(row => {
                if (!row.deleted && row.isNew) {
                    db.controllerQueryOneRow(request, response,
                        `insert into licenses.pay_rows values ($1, $2, $3, $4, $5)`,
                        [row.id_pay_row,
                            row.id_pay,
                            row.name,
                            row.serial_key,
                            row.sum
                        ]);
                } else if (!row.deleted && !row.isNew) {
                    db.controllerQueryOneRow(request, response,
                        `update licenses.pay_rows set 
                               name = $1, 
                               serial_key = $2, 
                               sum = $3
                             where id_pay_row = $4`,
                        [row.name,
                            row.serial_key,
                            row.sum,
                            row.id_pay_row
                        ]);
                } else if (!row.isNew && row.deleted) {
                    db.controllerQueryOneRow(request, response,
                        `delete from licenses.pay_rows where id_pay_row = $1`,
                        [row.id_pay_row]);
                }
            })
        }
    },

    removePay: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `delete from licenses.pay where id_pay = $1`,
            [request.params.id]);
    }

};
