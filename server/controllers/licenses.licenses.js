//Подключаем базу данных
const db = require('../db/db.controllers');

module.exports = {
    getLicenses: async (request, response) => {
        await db.controllerQuery(request, response,
            `select *,
                (select 
                    array_to_json(array_agg(p))::text
                 from 
                    licenses.license_plugins as lp 
                    inner join licenses.plugins as p
                        on p.id_plugin = lp.id_plugin
                 where 
                    lp.id_license = licenses.licenses.id_license) as plugins         
            from licenses.licenses 
            where id_client = $1 and licenses.licenses.deleted = false            
            order by licenses.licenses.sort_order`,
            [request.params.id]);
    },

    getLicense: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `select *,
                    (select 
                        array_to_json(array_agg(p))::text
                     from 
                        licenses.license_plugins as lp 
                        inner join licenses.plugins as p
                            on p.id_plugin = lp.id_plugin
                     where 
                        lp.id_license = licenses.licenses.id_license) as plugins   
                from licenses.licenses where id_license = $1`,
            [request.params.id]);
    },

    addLicense: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `call licenses.addlicense($1, $2, $3, $4, $5, $6)`,
            [request.body.id_license,
                request.body.id_client,
                request.body.serial_key,
                request.body.date_finish,
                request.body.active,
                request.body.plugins]);
    },

    updateLicense: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `call licenses.updatelicense($1, $2, $3, $4, $5)`,
            [request.body.id_license,
                request.body.serial_key,
                request.body.date_finish,
                request.body.active,
                request.body.plugins]);
    },

    removeLicense: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `update licenses.licenses set deleted = true where id_license = $1`,
            [request.params.id]);
    },

    // *******************************************************************

    getLicensePlugins: async (request, response) => {
        await db.controllerQuery(request, response,
            `select 
                    lic.id_license_plugin,
                    lic.id_license,
                    lic.id_plugin,
                    pl.sysname,
                    pl.name 
                from 
                    licenses.license_plugins as lic
                    left join licenses.plugins as pl on pl.id_plugin = lic.id_plugin                     
                where 
                    lic.id_license = $1`,
            [request.params.id]);
    },

    getLicensePlugin: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `select 
                    lic.id_license_plugin,
                    lic.id_license,
                    lic.id_plugin,
                    pl.sysname,
                    pl.name 
                from 
                    licenses.licenses_plugins as lic
                    left join licenses.plugins as pl on pl.id_plugin = lic.id_plugin                     
                where 
                    lic.id_license_plugin = $1`,
            [request.params.id]);
    },

    addLicensePlugin: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `insert into licenses.license_plugins (id_license_plugin, id_license, id_plugin)
                values ($1, $2, $3)`,
            [request.body.id_license_plugin,
                request.body.id_license,
                request.body.id_plugin]);
    },

    removeLicensePlugin: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `delete from licenses.license_plugins where id_license_plugin = $1`,
            [request.params.id]);
    }

};
