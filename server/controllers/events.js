//Подключаем обработчик ошибок
const errorHandler = require('../utils/errorHandler');

//Подключаем базу данных
const db = require('../db/index');

//Контроллер для выборки списка событий
module.exports.getEvents = async function(request, response) {
    try{
        console.log(request.body);
        await db.queryRows("select *, false as selected from uni.get_event_list($1, $2, $3, $4) where id_state<>0",
            [request.body.begin_time,
                   request.body.end_time,
                   request.body.parent,
                   request.body.json_filter], (error, result) => {
            if (error) {
                errorHandler(response, error)
            } else {
                response.status(200).json(result)
            }
        })
    } catch (error) {
        errorHandler(response, error)
    }
};
