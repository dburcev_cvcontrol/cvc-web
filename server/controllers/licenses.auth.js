//Подключаем обработчик ошибок
const errorHandler = require('../utils/errorHandler');

//Подключаем базу данных
const db = require('../db/index');

//Контроллер для пользователей
module.exports.login = async function(request, response) {
    try{
        console.log(request.body);
        await db.queryFirstRow("select * from core.get_user($1, $2)",
            [request.body.login,
                request.body.password], (error, result) => {
                if (error) {
                    errorHandler(response, error);
                } else if (!result) {
                    response.status(400).json({error: 'Пользователь не найден'})
                } else if (!result.active) {
                    response.status(401).json({error: 'Пользователь не активен'})
                } else {
                    response.status(200).json(result)
                }
            })
    } catch (error) {
        errorHandler(response, error)
    }
};
