//Подключаем базу данных
const db = require('../db/db.controllers');

// *******************************************************************

module.exports = {
    getGroups: async (request, response) => {
        await db.controllerQuery(request, response,
            `select * from licenses.client_groups`, []);
    },

    getGroup: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `select * from licenses.client_groups where id_group = $1`,
            [request.params.id]);
    },

    addGroup: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `insert into licenses.client_groups values($1, $2)`,
            [request.body.id_group,
            request.body.name]);
    },

    updateGroup: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `update licenses.client_groups set
                    name = $1
               where id_group = $2`,
            [request.body.name,
                request.body.id_group]);
    },

    removeGroup: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `delete from licenses.client_groups where id_group = $1`,
            [request.params.id]);
    }
};

