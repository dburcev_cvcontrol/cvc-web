//Подключаем базу данных
const db = require('../db/db.controllers');

module.exports = {
    getPlugins: async (request, response) => {
        await db.controllerQuery(request, response,
            `select * from licenses.plugins`, []);
    },

    getPlugin: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `select * from licenses.plugins where id_plugin = $1`,
            [request.params.id]);
    },

    addPlugin: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `insert into licenses.plugins values ($1, $2, $3, $4)`,
            [request.body.id_plugin,
            request.body.sysname,
            request.body.name,
            request.body.comment]);
    },

    updatePlugin: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `update licenses.plugins set
                    sysname = $1,
                    name = $2,
                    comment = $3
                where id_plugin = $4`,
            [request.body.sysname,
            request.body.name,
            request.body.comment,
            request.body.id_plugin]);
    },

    removePlugin: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `delete from licenses.plugins where id_plugin = $1`,
            [request.params.id]);
    }
};
