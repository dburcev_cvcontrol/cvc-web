//Подключаем базу данных
const db = require('../db/db.controllers');

module.exports = {
    getClients: async (request, response) => {
        const idGroup = request.params.id;

        if (idGroup === 'all') {
            await db.controllerQuery(request, response,
                `select * from licenses.clients`, []);
        } else {
            await db.controllerQuery(request, response,
                `select * from licenses.clients where id_group = $1`, [idGroup]);
        }
    },

    getClient: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `select * from licenses.clients where id_client = $1`,
            [request.params.id]);
    },

    addClient: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `insert into licenses.clients (id_client, id_group, name, json_data)
                values($1, $2, $3, $4);`,
            [request.body.id_client,
                request.body.id_group,
                request.body.name,
                request.body.json_data]);
    },

    updateClient: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `update licenses.clients set
                    id_group = $1,
                    name = $2,
                    json_data = $3
                 where
                    id_client = $4`,
            [request.body.id_group,
                request.body.name,
                request.body.json_data,
                request.body.id_client]);
    },

    removeClient: async (request, response) => {
        await db.controllerQueryOneRow(request, response,
            `delete from licenses.clients where id_client = $1`,
            [request.params.id]);
    }
};
