const { Pool } = require('pg');
const env = require('../config/env');
const pool = new Pool(env.database);

module.exports = {
    /**
        Выполнение запроса в базу данных
        @text {string} текст запроса (в качестве параметров указывается $N, где N порядковый номер
        @params {Array} массив передаваемых параметров
        @callback {function(err, res)} - где err - ошибка выполнения запроса, res - результат выполнения в виде JSON объекта
     */
    query: (text, params, callback) => {
        const start = Date.now();
        return pool.query(text, params, (err, res) => {
            const duration = Date.now() - start;

            if (res) {
                console.log('executed query', {text, duration, rows: res.rowCount});
                callback(err, res.rows)
            } else {
                console.log('executed query', {detail: err.detail, where: err.where});
                callback(err, null);
            }
        })
    },
    /**
     Выполнение запроса в базу данных
     @text {string} текст запроса (в качестве параметров указывается $N, где N порядковый номер
     @params {Array} массив передаваемых параметров
     @callback {function(err, res)} - где err - ошибка выполнения запроса, res - результат выполнения в виде массива объектов, в котором каждая строка - JSON объект
     */
    queryRows: (text, params, callback) => {
        const start = Date.now();
        return pool.query(text, params, (err, res) => {
            const duration = Date.now() - start;

            if (res) {
                // console.log('executed query', { text, duration, rows: res.rowCount });
                callback(err, res.rows)
            } else {
                console.log('executed query', {detail: err.detail, where: err.where});
                callback(err, null);
            }
        })
    },
    /**
     Выполнение запроса в базу данных
     @text {string} текст запроса (в качестве параметров указывается $N, где N порядковый номер
     @params {Array} массив передаваемых параметров
     @callback {function(err, res)} - где err - ошибка выполнения запроса, res - результат выполнения в виде JSON объекта, первой строчки
     */
    queryFirstRow: (text, params, callback) => {
        const start = Date.now();
        return pool.query(text, params, (err, res) => {
            const duration = Date.now() - start;
            if (res) {
                // console.log('executed query', { text, duration, rows: res.rowCount });
                callback(err, res.rows[0])
            } else {
                console.log('SQL: ' + text);
                console.log('params: ' + params);
                console.log('executed query', {error:err.error, hint:err.hint, detail: err.detail, where: err.where, all: err});
                callback(err, null);
            }
        })
    },
    getClient: (callback) => {
        pool.connect((err, client, done) => {
            const query = client.query;
            // monkey patch the query method to keep track of the last query executed
            client.query = (...args) => {
                client.lastQuery = args;
                return query.apply(client, args)
            };
            // set a timeout of 5 seconds, after which we will log this client's last query
            const timeout = setTimeout(() => {
                console.error('A client has been checked out for more than 5 seconds!');
                console.error(`The last executed query on this client was: ${client.lastQuery}`)
            }, 5000);
            const release = (err) => {
                // call the actual 'done' method, returning this client to the pool
                done(err);
                // clear our timeout
                clearTimeout(timeout);
                // set the query method back to its old un-monkey-patched version
                client.query = query
            };
            callback(err, client, release)
        })
    }
};
