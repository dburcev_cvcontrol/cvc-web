//Подключаем обработчик ошибок
const errorHandler = require('../utils/errorHandler');

//Подключаем базу данных
const db = require('../db/index');

module.exports = {
//Контроллер для клиентов
    controllerQuery: async (request, response, sql, params) => {
        try {
            await db.queryRows(sql, params, (error, result) => {
                if (error) {
                    errorHandler(response, error)
                } else {
                    // console.log(result);
                    response.status(200).json(result)
                }
            })
        } catch (error) {
            errorHandler(response, error)
        }
    },

    controllerQueryOneRow: async (request, response, sql, params) => {
        try {
            await db.queryFirstRow(sql, params, (error, result) => {
                if (error) {
                    errorHandler(response, error)
                } else {
                    response.status(200).json(result)
                }
            })
        } catch (error) {
            errorHandler(response, error)
        }
    }
};
