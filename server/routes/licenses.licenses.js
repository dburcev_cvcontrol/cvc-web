//Роуты обработки лицензий

//Подключаем основной фреймфорк
const express = require('express');

//Подключаем контроллеры для событий
const controller = require('../controllers/licenses.licenses');
const router = express.Router();

//Описываем роуты
router.get('/:id', controller.getLicenses);
router.get('/detail/:id', controller.getLicense);
router.post('/', controller.addLicense);
router.put('/', controller.updateLicense);
router.delete('/:id', controller.removeLicense);

router.get('/plugins/:id', controller.getLicensePlugins);
router.get('/plugins/detail/:id', controller.getLicensePlugin);
router.post('/plugins', controller.addLicensePlugin);
router.delete('/plugins/:id', controller.removeLicensePlugin);

module.exports = router;
