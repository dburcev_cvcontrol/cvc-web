//Роуты обработки событий

//Подключаем основной фреймфорк
const express = require('express');

//Подключаем контроллеры для событий
const controller = require('../controllers/uni');
const router = express.Router();

//Описываем роуты
router.post('/', controller.getSmartFilters);

module.exports = router;
