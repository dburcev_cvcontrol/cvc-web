//Роуты обработки плагинов (модулей)

//Подключаем основной фреймфорк
const express = require('express');

//Подключаем контроллеры для событий
const controller = require('../controllers/licenses.plugins');
const router = express.Router();

//Описываем роуты
router.get('/', controller.getPlugins);
router.get('/detail/:id', controller.getPlugin);
router.post('/', controller.addPlugin);
router.put('/', controller.updatePlugin);
router.delete('/:id', controller.removePlugin);

module.exports = router;
