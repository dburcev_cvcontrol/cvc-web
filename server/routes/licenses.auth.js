//Роуты обработки событий

//Подключаем основной фреймфорк
const express = require('express');

//Подключаем контроллеры для событий
const controller = require('../controllers/licenses.auth');
const router = express.Router();

//Описываем роуты
router.post('/', controller.login);

module.exports = router;
