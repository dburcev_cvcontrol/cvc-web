//Роуты обработки оплаты

//Подключаем основной фреймфорк
const express = require('express');

//Подключаем контроллеры для событий
const controller = require('../controllers/licenses.pay');
const router = express.Router();

//Описываем роуты
router.get('/:id', controller.getPays);
router.get('/detail/:id', controller.getPay);
router.post('/', controller.addPay);
router.put('/', controller.updatePay);
router.delete('/:id', controller.removePay);

module.exports = router;
