//Роуты обработки групп клиентов

//Подключаем основной фреймфорк
const express = require('express');

//Подключаем контроллеры для событий
const controller = require('../controllers/licenses.groups');
const router = express.Router();

//Описываем роуты
router.get('/', controller.getGroups);
router.get('/detail/:id', controller.getGroup);
router.post('/', controller.addGroup);
router.put('/', controller.updateGroup);
router.delete('/:id', controller.removeGroup);

module.exports = router;
