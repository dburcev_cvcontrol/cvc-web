//Роуты обработки клиентов

//Подключаем основной фреймфорк
const express = require('express');

//Подключаем контроллеры для событий
const controller = require('../controllers/licenses.clients');
const router = express.Router();

//Описываем роуты
router.get('/:id', controller.getClients);
router.get('/detail/:id', controller.getClient);
router.post('/', controller.addClient);
router.put('/', controller.updateClient);
router.delete('/:id', controller.removeClient);

module.exports = router;
