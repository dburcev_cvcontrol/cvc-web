//Подключение фреймфорка
const express = require('express');
const app = express();

//Пакет для парсинга полученных результатов
//для того чтобы можно было обратиться к получаемым данным как к JSON
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

//Пакет для возможности подключения к другим доменам
const cors = require('cors');
app.use(cors());

//Локальный логгер для отображения получаемыз запросов от клиентов
const morgan = require('morgan');
app.use(morgan('tiny'));

//Подключаем конфигурационные данные
const env = require('./config/env');

//Определяем роут для событий
const routerEvents = require('./routes/events');
app.use('/api/events', routerEvents);

const routerUni = require('./routes/uni');
app.use('/api/uni', routerUni);

const routerLicenseAuth = require('./routes/licenses.auth');
app.use('/api/auth', routerLicenseAuth);

const routerLicenseGroups = require('./routes/licenses.groups');
app.use('/api/groups', routerLicenseGroups);

const routerLicenseClients = require('./routes/licenses.clients');
app.use('/api/clients', routerLicenseClients);

const routerLicenseLicenses = require('./routes/licenses.licenses');
app.use('/api/licenses', routerLicenseLicenses);

const routerLicensePlugins = require('./routes/licenses.plugins');
app.use('/api/plugins', routerLicensePlugins);

const routerLicensePay = require('./routes/licenses.pay');
app.use('/api/pay', routerLicensePay);

const routerHelp = require('./routes/help');
app.use('/', routerHelp);

//Запускаем сервер на прослушку
app.listen(env.webserver.port, env.webserver.address, function () {
    console.log("Сервер запущен http://%s:%s", env.webserver.address, env.webserver.port);
});


