import { Component } from '@angular/core';
import {EventsService} from './services/events.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  filterDate: Date;

  constructor(public eventsService: EventsService) {
    this.eventsService.filterDate.subscribe( value => {
      this.filterDate = value;
    });
  }

  onDateClick($event: any) {
    this.eventsService.filterDate.next(
      new Date($event.target.value)
    );
  }
}
