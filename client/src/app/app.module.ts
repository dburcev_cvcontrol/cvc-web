import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { EventsComponent } from './events/events.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { FilterComponent } from './filter/filter/filter.component';
import {NgMetro4Module} from 'ng-metro4';
import { SmartFitlersComponent } from './events/smart-fitlers/smart-fitlers.component';

@NgModule({
  declarations: [
    AppComponent,
    EventsComponent,
    FilterComponent,
    SmartFitlersComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgMetro4Module
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
