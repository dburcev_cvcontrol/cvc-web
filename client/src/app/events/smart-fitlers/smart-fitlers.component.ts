import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {EventListItem} from '../events.component';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {checkIfGenericTypesAreUnbound} from '@angular/compiler-cli/src/ngtsc/typecheck/src/ts_util';

export interface SmartFilterItem {
  id_smart_filter: number;
  name: string;
  short: string;
  json_data: string;
  sort_order: number;
  id_parent: number;
  level: number;
  expanded: boolean;
  visible: boolean;
  hasChildren: boolean;
  active: boolean;
}

@Component({
  selector: 'app-smart-fitlers',
  templateUrl: './smart-fitlers.component.html',
  styleUrls: ['./smart-fitlers.component.css']
})
export class SmartFitlersComponent implements OnInit {

  loadedSmartFilters: SmartFilterItem[] = []; // Исходный не обработанный смартфильтр
  smartFilters: SmartFilterItem[] = []; // Обработанный смартфильтр
  dataLoaded = false;

  @ViewChild('tree', {static: false}) treeRef: ElementRef;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.doLoad();
  }

  doLoad() {
    this.dataLoaded = false;
    this.http.post<SmartFilterItem[]>(`http://${environment.cvcServer}/api/uni/`, {
      keyUser: 1
    })
      .subscribe(response => {
        this.loadedSmartFilters = response;
        this.buildTreeArray(0, 0);
        this.dataLoaded = true;
      });
  }

  getItemLevel(idSmartFitler: number): number {
    const findItem = this.smartFilters.find(item => item.id_smart_filter === idSmartFitler);

    if (findItem) {
      return findItem.level + 1;
    } else {
      return 0;
    }
  }

  getShortName(fullName): string {
    const words = fullName.split(' ');
    let result = '';
    if (words.length > 0) {
      result = words[0].charAt(0).toUpperCase();
      if (words.length > 1) {
        result = result + words[1].charAt(0).toUpperCase();
      }
    }
    return result;
  }

  buildTreeArray(root: number, parentLevel: number): number {
    let result = 0;
    this.loadedSmartFilters.forEach((item, index, array) => {
      if (item.id_parent === root) {
        result ++;
        if (root === 0) {
          item.level = 0;
          item.visible = true;
        } else {
          item.level = parentLevel + 1;
          item.visible = false;
        }
        item.active = false;
        const json = JSON.parse(item.json_data);
        if (json.shortName) {
          item.short = json.shortName;
        } else {
          item.short = this.getShortName(item.name);
        }
        this.smartFilters.push(item);
        item.hasChildren = (this.buildTreeArray(item.id_smart_filter, item.level) > 0);
      }
    });
    return result;
  }

  treeExpandButtonClick(idItem: number) {
    this.smartFilters.forEach((item, index, array) => {
      if (item.id_smart_filter === idItem) {
        item.expanded = !item.expanded;
      } else if (item.id_parent === idItem) {
        item.visible = !item.visible;
        if (!item.visible) {
          if (item.expanded) {
            this.treeExpandButtonClick(item.id_smart_filter);
          }
        }
      }
    });
  }

  treeItemClick(idSmartFilter: number) {
    this.smartFilters.forEach((item, index, array) => {
      item.active = (item.id_smart_filter === idSmartFilter);
    });
  }
}
