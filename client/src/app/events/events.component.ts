import {Component, Input, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EventsService} from '../services/events.service';
import {environment} from '../../environments/environment';

export interface EventListItem {
  id_event: number;
  begin_time: string;
  end_time: string;
  period_time: number;
  id_camera: number;
  camname: string;
  id_event_type: number;
  eventtypename: string;
  eventcolor: number;
  verified: boolean;
  violation_comment: string;
  id_camera_ext: number;
  external_object: string;
  event_data: string;
  id_state: number;
  id_parent: number;
  has_child: boolean;
  ranges?: string;
  closed: boolean;
  selected: boolean;
}

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  events: EventListItem[] = [];
  dataLoaded = false;
  filterDateBegin: number;
  filterDateEnd: number;

  constructor(
    private http: HttpClient,
    private eventsService: EventsService ) {
      this.eventsService.filterDate.subscribe( value => {
        this.filterDateBegin = Math.floor(value.getTime() / 1000);
        this.filterDateEnd = Math.floor(value.getTime() / 1000) + 86399;
        this.doLoadData();
      });
  }

  ngOnInit() {
    // this.doLoadData();
  }

  doLoadData() {
    this.dataLoaded = false;
    this.http.post<EventListItem[]>(`http://${environment.cvcServer}/api/events`, {
      begin_time: this.filterDateBegin,
      end_time: this.filterDateEnd,
      parent: 0,
      json_filter: '',
    })
      .subscribe(response => {
        this.events = response;
        this.dataLoaded = true;
      });
  }

  onEventClick(event: MouseEvent, idEvent: number) {
    this.events.forEach((item, index, array) => {
      // ... делать что-то с item
      item.selected = ((event.ctrlKey) && (item.selected)) || (item.id_event === idEvent);
      // item.closed = item.closed || item.selected;
    });
    this.eventsService.eventId = idEvent;
  }

  onStateClick($event: MouseEvent, idEvent: number, idState: number) {
    this.events.forEach((item, index, array) => {
      // ... делать что-то с item
      if (item.id_event === idEvent) {
        if (item.id_state === idState) {
          item.id_state = 0;
        } else {
          item.id_state = idState;
        }
      }
    });
  }
}
