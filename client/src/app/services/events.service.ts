import {Injectable} from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventsService {
    eventId = 0;
    filterDate: BehaviorSubject<Date> = new BehaviorSubject<Date>(new Date());

    constructor() {
    }
}
