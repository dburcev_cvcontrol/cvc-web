import { Component, OnInit } from '@angular/core';
import {EventsService} from '../../services/events.service';
import {FormControl, FormGroup} from '@angular/forms';
import {CalendarComponent, M4FormControl, M4FormGroup} from 'ng-metro4';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

  public formGroup: FormGroup;

  constructor(private eventsService: EventsService) {
    this.formGroup = new M4FormGroup('reactive_demo', {
      filterDate: new M4FormControl(CalendarComponent)
    });
  }

  ngOnInit() {
  }

  onFilterClick() {
    console.log(this.formGroup.value);
  }
}
