PGDMP     %                     x         	   licensing    11.4    11.2 d    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    185383 	   licensing    DATABASE     �   CREATE DATABASE licensing WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';
    DROP DATABASE licensing;
             postgres    false                        2615    185394    core    SCHEMA        CREATE SCHEMA core;
    DROP SCHEMA core;
             postgres    false            
            2615    185384    licenses    SCHEMA        CREATE SCHEMA licenses;
    DROP SCHEMA licenses;
             postgres    false            �            1255    185735    get_user(text, text)    FUNCTION     V  CREATE FUNCTION core.get_user(_login text, _password text) RETURNS TABLE(id_user integer, login text, name text, comment text, active boolean, superuser boolean, rights text)
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select
        u.id_user,
        u.login,
        u."name",
        u."comment",
        u.active,
        u.superuser, 
        (select 
            array_to_json(array_agg(r.sysname))::text
         from 
            core.user_roles ur 
            inner join core.role_rights as rr
                on rr.id_role = ur.id_role 
            inner join core.rights r
                on r.id_right = rr.id_right
         where 
            ur.id_user = u.id_user ) as rights
    from
        core.users u 
    where 
        u.login = _login 
        and (u."password" is null or md5(_password) = u."password" );
END;
$$;
 :   DROP FUNCTION core.get_user(_login text, _password text);
       core       postgres    false    8            �            1259    185468    json_params    TABLE     {   CREATE TABLE core.json_params (
    id_json_param integer NOT NULL,
    id_table integer NOT NULL,
    param_data jsonb
);
    DROP TABLE core.json_params;
       core         postgres    false    8            �           0    0    TABLE json_params    COMMENT     �   COMMENT ON TABLE core.json_params IS 'Дополнительные параметры для описания записей в таблицах';
            core       postgres    false    206            �            1259    185466    json_params_id_json_param_seq    SEQUENCE     �   CREATE SEQUENCE core.json_params_id_json_param_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE core.json_params_id_json_param_seq;
       core       postgres    false    8    206            �           0    0    json_params_id_json_param_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE core.json_params_id_json_param_seq OWNED BY core.json_params.id_json_param;
            core       postgres    false    205            �            1259    185480    json_values    TABLE     �   CREATE TABLE core.json_values (
    id_json_value integer NOT NULL,
    id_table integer NOT NULL,
    id_record bigint NOT NULL,
    value_data jsonb
);
    DROP TABLE core.json_values;
       core         postgres    false    8            �           0    0    TABLE json_values    COMMENT     �   COMMENT ON TABLE core.json_values IS 'Значения дополнительных параметров для каждой записи любой таблицы';
            core       postgres    false    208            �            1259    185478    json_values_id_json_value_seq    SEQUENCE     �   CREATE SEQUENCE core.json_values_id_json_value_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE core.json_values_id_json_value_seq;
       core       postgres    false    8    208            �           0    0    json_values_id_json_value_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE core.json_values_id_json_value_seq OWNED BY core.json_values.id_json_value;
            core       postgres    false    207            �            1259    185439    log    TABLE     �   CREATE TABLE core.log (
    id_log bigint NOT NULL,
    id_table integer,
    id_record bigint,
    event text,
    event_data jsonb,
    date_time timestamp with time zone,
    id_user integer NOT NULL
);
    DROP TABLE core.log;
       core         postgres    false    8            �           0    0 	   TABLE log    COMMENT     >   COMMENT ON TABLE core.log IS 'Журнал действий';
            core       postgres    false    204            �            1259    185437    log_id_log_seq    SEQUENCE     u   CREATE SEQUENCE core.log_id_log_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE core.log_id_log_seq;
       core       postgres    false    204    8            �           0    0    log_id_log_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE core.log_id_log_seq OWNED BY core.log.id_log;
            core       postgres    false    203            �            1259    185681    rights    TABLE     f   CREATE TABLE core.rights (
    id_right integer NOT NULL,
    sysname text NOT NULL,
    name text
);
    DROP TABLE core.rights;
       core         postgres    false    8            �           0    0    TABLE rights    COMMENT     =   COMMENT ON TABLE core.rights IS 'Права доступа';
            core       postgres    false    210            �            1259    185679    rights_id_right_seq    SEQUENCE     �   CREATE SEQUENCE core.rights_id_right_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE core.rights_id_right_seq;
       core       postgres    false    8    210            �           0    0    rights_id_right_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE core.rights_id_right_seq OWNED BY core.rights.id_right;
            core       postgres    false    209            �            1259    185701    role_rights    TABLE     _   CREATE TABLE core.role_rights (
    id_role integer NOT NULL,
    id_right integer NOT NULL
);
    DROP TABLE core.role_rights;
       core         postgres    false    8            �           0    0    TABLE role_rights    COMMENT     =   COMMENT ON TABLE core.role_rights IS 'Права роли	';
            core       postgres    false    213            �            1259    185692    roles    TABLE     R   CREATE TABLE core.roles (
    id_role integer NOT NULL,
    name text NOT NULL
);
    DROP TABLE core.roles;
       core         postgres    false    8            �           0    0    TABLE roles    COMMENT     ,   COMMENT ON TABLE core.roles IS 'Роли	';
            core       postgres    false    212            �            1259    185690    roles_id_role_seq    SEQUENCE     �   CREATE SEQUENCE core.roles_id_role_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE core.roles_id_role_seq;
       core       postgres    false    8    212            �           0    0    roles_id_role_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE core.roles_id_role_seq OWNED BY core.roles.id_role;
            core       postgres    false    211            �            1259    185428    tables    TABLE     f   CREATE TABLE core.tables (
    id_table integer NOT NULL,
    sysname text NOT NULL,
    name text
);
    DROP TABLE core.tables;
       core         postgres    false    8            �           0    0    TABLE tables    COMMENT     =   COMMENT ON TABLE core.tables IS 'Список таблиц';
            core       postgres    false    202            �            1259    185426    tables_id_table_seq    SEQUENCE     �   CREATE SEQUENCE core.tables_id_table_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE core.tables_id_table_seq;
       core       postgres    false    202    8            �           0    0    tables_id_table_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE core.tables_id_table_seq OWNED BY core.tables.id_table;
            core       postgres    false    201            �            1259    185716 
   user_roles    TABLE     ]   CREATE TABLE core.user_roles (
    id_user integer NOT NULL,
    id_role integer NOT NULL
);
    DROP TABLE core.user_roles;
       core         postgres    false    8            �           0    0    TABLE user_roles    COMMENT     I   COMMENT ON TABLE core.user_roles IS 'Роли пользователя';
            core       postgres    false    214            �            1259    185417    users    TABLE     �   CREATE TABLE core.users (
    id_user integer NOT NULL,
    login text NOT NULL,
    password text,
    name text,
    comment text,
    active boolean DEFAULT false,
    superuser boolean DEFAULT false
);
    DROP TABLE core.users;
       core         postgres    false    8            �           0    0    TABLE users    COMMENT     J   COMMENT ON TABLE core.users IS 'Список пользователей';
            core       postgres    false    200            �            1259    185415    users_id_user_seq    SEQUENCE     �   CREATE SEQUENCE core.users_id_user_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE core.users_id_user_seq;
       core       postgres    false    200    8            �           0    0    users_id_user_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE core.users_id_user_seq OWNED BY core.users.id_user;
            core       postgres    false    199            �            1259    193976    client_groups    TABLE     ~   CREATE TABLE licenses.client_groups (
    id_group uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL
);
 #   DROP TABLE licenses.client_groups;
       licenses         postgres    false    10            �            1259    193996    clients    TABLE     �   CREATE TABLE licenses.clients (
    id_client uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    id_group uuid NOT NULL,
    name text NOT NULL,
    json_data text
);
    DROP TABLE licenses.clients;
       licenses         postgres    false    10            �            1259    194035    license_plugins    TABLE     �   CREATE TABLE licenses.license_plugins (
    id_license_plugin uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    id_license uuid NOT NULL,
    id_plugin uuid NOT NULL
);
 %   DROP TABLE licenses.license_plugins;
       licenses         postgres    false    10            �            1259    194004    licenses    TABLE     �   CREATE TABLE licenses.licenses (
    id_license uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    id_client uuid NOT NULL,
    serial_key text NOT NULL,
    date_finish integer,
    active boolean DEFAULT false
);
    DROP TABLE licenses.licenses;
       licenses         postgres    false    10            �            1259    194026    plugins    TABLE     �   CREATE TABLE licenses.plugins (
    id_plugin uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    sysname text NOT NULL,
    name text NOT NULL,
    comment text
);
    DROP TABLE licenses.plugins;
       licenses         postgres    false    10            �
           2604    185471    json_params id_json_param    DEFAULT     �   ALTER TABLE ONLY core.json_params ALTER COLUMN id_json_param SET DEFAULT nextval('core.json_params_id_json_param_seq'::regclass);
 F   ALTER TABLE core.json_params ALTER COLUMN id_json_param DROP DEFAULT;
       core       postgres    false    205    206    206            �
           2604    185483    json_values id_json_value    DEFAULT     �   ALTER TABLE ONLY core.json_values ALTER COLUMN id_json_value SET DEFAULT nextval('core.json_values_id_json_value_seq'::regclass);
 F   ALTER TABLE core.json_values ALTER COLUMN id_json_value DROP DEFAULT;
       core       postgres    false    208    207    208            �
           2604    185442 
   log id_log    DEFAULT     d   ALTER TABLE ONLY core.log ALTER COLUMN id_log SET DEFAULT nextval('core.log_id_log_seq'::regclass);
 7   ALTER TABLE core.log ALTER COLUMN id_log DROP DEFAULT;
       core       postgres    false    204    203    204            �
           2604    185684    rights id_right    DEFAULT     n   ALTER TABLE ONLY core.rights ALTER COLUMN id_right SET DEFAULT nextval('core.rights_id_right_seq'::regclass);
 <   ALTER TABLE core.rights ALTER COLUMN id_right DROP DEFAULT;
       core       postgres    false    210    209    210            �
           2604    185695    roles id_role    DEFAULT     j   ALTER TABLE ONLY core.roles ALTER COLUMN id_role SET DEFAULT nextval('core.roles_id_role_seq'::regclass);
 :   ALTER TABLE core.roles ALTER COLUMN id_role DROP DEFAULT;
       core       postgres    false    212    211    212            �
           2604    185431    tables id_table    DEFAULT     n   ALTER TABLE ONLY core.tables ALTER COLUMN id_table SET DEFAULT nextval('core.tables_id_table_seq'::regclass);
 <   ALTER TABLE core.tables ALTER COLUMN id_table DROP DEFAULT;
       core       postgres    false    201    202    202            �
           2604    185420    users id_user    DEFAULT     j   ALTER TABLE ONLY core.users ALTER COLUMN id_user SET DEFAULT nextval('core.users_id_user_seq'::regclass);
 :   ALTER TABLE core.users ALTER COLUMN id_user DROP DEFAULT;
       core       postgres    false    199    200    200            �          0    185468    json_params 
   TABLE DATA               H   COPY core.json_params (id_json_param, id_table, param_data) FROM stdin;
    core       postgres    false    206            �          0    185480    json_values 
   TABLE DATA               S   COPY core.json_values (id_json_value, id_table, id_record, value_data) FROM stdin;
    core       postgres    false    208            �          0    185439    log 
   TABLE DATA               _   COPY core.log (id_log, id_table, id_record, event, event_data, date_time, id_user) FROM stdin;
    core       postgres    false    204            �          0    185681    rights 
   TABLE DATA               7   COPY core.rights (id_right, sysname, name) FROM stdin;
    core       postgres    false    210            �          0    185701    role_rights 
   TABLE DATA               6   COPY core.role_rights (id_role, id_right) FROM stdin;
    core       postgres    false    213            �          0    185692    roles 
   TABLE DATA               ,   COPY core.roles (id_role, name) FROM stdin;
    core       postgres    false    212            �          0    185428    tables 
   TABLE DATA               7   COPY core.tables (id_table, sysname, name) FROM stdin;
    core       postgres    false    202            �          0    185716 
   user_roles 
   TABLE DATA               4   COPY core.user_roles (id_user, id_role) FROM stdin;
    core       postgres    false    214            �          0    185417    users 
   TABLE DATA               Y   COPY core.users (id_user, login, password, name, comment, active, superuser) FROM stdin;
    core       postgres    false    200            �          0    193976    client_groups 
   TABLE DATA               9   COPY licenses.client_groups (id_group, name) FROM stdin;
    licenses       postgres    false    215            �          0    193996    clients 
   TABLE DATA               I   COPY licenses.clients (id_client, id_group, name, json_data) FROM stdin;
    licenses       postgres    false    216            �          0    194035    license_plugins 
   TABLE DATA               U   COPY licenses.license_plugins (id_license_plugin, id_license, id_plugin) FROM stdin;
    licenses       postgres    false    219            �          0    194004    licenses 
   TABLE DATA               \   COPY licenses.licenses (id_license, id_client, serial_key, date_finish, active) FROM stdin;
    licenses       postgres    false    217            �          0    194026    plugins 
   TABLE DATA               F   COPY licenses.plugins (id_plugin, sysname, name, comment) FROM stdin;
    licenses       postgres    false    218            �           0    0    json_params_id_json_param_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('core.json_params_id_json_param_seq', 1, false);
            core       postgres    false    205            �           0    0    json_values_id_json_value_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('core.json_values_id_json_value_seq', 1, false);
            core       postgres    false    207            �           0    0    log_id_log_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('core.log_id_log_seq', 1, false);
            core       postgres    false    203            �           0    0    rights_id_right_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('core.rights_id_right_seq', 4, true);
            core       postgres    false    209            �           0    0    roles_id_role_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('core.roles_id_role_seq', 4, true);
            core       postgres    false    211            �           0    0    tables_id_table_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('core.tables_id_table_seq', 4, true);
            core       postgres    false    201            �           0    0    users_id_user_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('core.users_id_user_seq', 8, true);
            core       postgres    false    199            �
           2606    185476    json_params json_params_pk 
   CONSTRAINT     a   ALTER TABLE ONLY core.json_params
    ADD CONSTRAINT json_params_pk PRIMARY KEY (id_json_param);
 B   ALTER TABLE ONLY core.json_params DROP CONSTRAINT json_params_pk;
       core         postgres    false    206            �
           2606    185488    json_values json_values_pk 
   CONSTRAINT     a   ALTER TABLE ONLY core.json_values
    ADD CONSTRAINT json_values_pk PRIMARY KEY (id_json_value);
 B   ALTER TABLE ONLY core.json_values DROP CONSTRAINT json_values_pk;
       core         postgres    false    208            �
           2606    185447 
   log log_pk 
   CONSTRAINT     J   ALTER TABLE ONLY core.log
    ADD CONSTRAINT log_pk PRIMARY KEY (id_log);
 2   ALTER TABLE ONLY core.log DROP CONSTRAINT log_pk;
       core         postgres    false    204            �
           2606    185689    rights rights_pk 
   CONSTRAINT     R   ALTER TABLE ONLY core.rights
    ADD CONSTRAINT rights_pk PRIMARY KEY (id_right);
 8   ALTER TABLE ONLY core.rights DROP CONSTRAINT rights_pk;
       core         postgres    false    210            �
           2606    185705    role_rights role_rights_pk 
   CONSTRAINT     e   ALTER TABLE ONLY core.role_rights
    ADD CONSTRAINT role_rights_pk PRIMARY KEY (id_role, id_right);
 B   ALTER TABLE ONLY core.role_rights DROP CONSTRAINT role_rights_pk;
       core         postgres    false    213    213            �
           2606    185700    roles roles_pk 
   CONSTRAINT     O   ALTER TABLE ONLY core.roles
    ADD CONSTRAINT roles_pk PRIMARY KEY (id_role);
 6   ALTER TABLE ONLY core.roles DROP CONSTRAINT roles_pk;
       core         postgres    false    212            �
           2606    185436    tables tables_pk 
   CONSTRAINT     R   ALTER TABLE ONLY core.tables
    ADD CONSTRAINT tables_pk PRIMARY KEY (id_table);
 8   ALTER TABLE ONLY core.tables DROP CONSTRAINT tables_pk;
       core         postgres    false    202            �
           2606    185720    user_roles user_roles_pk 
   CONSTRAINT     b   ALTER TABLE ONLY core.user_roles
    ADD CONSTRAINT user_roles_pk PRIMARY KEY (id_user, id_role);
 @   ALTER TABLE ONLY core.user_roles DROP CONSTRAINT user_roles_pk;
       core         postgres    false    214    214            �
           2606    185425    users users_pk 
   CONSTRAINT     O   ALTER TABLE ONLY core.users
    ADD CONSTRAINT users_pk PRIMARY KEY (id_user);
 6   ALTER TABLE ONLY core.users DROP CONSTRAINT users_pk;
       core         postgres    false    200            �
           2606    193983    client_groups client_groups_pk 
   CONSTRAINT     d   ALTER TABLE ONLY licenses.client_groups
    ADD CONSTRAINT client_groups_pk PRIMARY KEY (id_group);
 J   ALTER TABLE ONLY licenses.client_groups DROP CONSTRAINT client_groups_pk;
       licenses         postgres    false    215            �
           2606    194013    clients clients_pk 
   CONSTRAINT     Y   ALTER TABLE ONLY licenses.clients
    ADD CONSTRAINT clients_pk PRIMARY KEY (id_client);
 >   ALTER TABLE ONLY licenses.clients DROP CONSTRAINT clients_pk;
       licenses         postgres    false    216                       2606    194040 "   license_plugins license_plugins_pk 
   CONSTRAINT     q   ALTER TABLE ONLY licenses.license_plugins
    ADD CONSTRAINT license_plugins_pk PRIMARY KEY (id_license_plugin);
 N   ALTER TABLE ONLY licenses.license_plugins DROP CONSTRAINT license_plugins_pk;
       licenses         postgres    false    219            �
           2606    194020    licenses licenses_pk 
   CONSTRAINT     \   ALTER TABLE ONLY licenses.licenses
    ADD CONSTRAINT licenses_pk PRIMARY KEY (id_license);
 @   ALTER TABLE ONLY licenses.licenses DROP CONSTRAINT licenses_pk;
       licenses         postgres    false    217                        2606    194034    plugins plugins_pk 
   CONSTRAINT     Y   ALTER TABLE ONLY licenses.plugins
    ADD CONSTRAINT plugins_pk PRIMARY KEY (id_plugin);
 >   ALTER TABLE ONLY licenses.plugins DROP CONSTRAINT plugins_pk;
       licenses         postgres    false    218            �
           1259    185477    json_params_id_table_idx    INDEX     Y   CREATE UNIQUE INDEX json_params_id_table_idx ON core.json_params USING btree (id_table);
 *   DROP INDEX core.json_params_id_table_idx;
       core         postgres    false    206            �
           1259    185453    log_id_record_idx    INDEX     D   CREATE INDEX log_id_record_idx ON core.log USING btree (id_record);
 #   DROP INDEX core.log_id_record_idx;
       core         postgres    false    204                       2606    185546    json_params json_params_fk    FK CONSTRAINT     �   ALTER TABLE ONLY core.json_params
    ADD CONSTRAINT json_params_fk FOREIGN KEY (id_table) REFERENCES core.tables(id_table) ON UPDATE CASCADE ON DELETE CASCADE;
 B   ALTER TABLE ONLY core.json_params DROP CONSTRAINT json_params_fk;
       core       postgres    false    202    2792    206                       2606    185489    json_values json_values_fk    FK CONSTRAINT     �   ALTER TABLE ONLY core.json_values
    ADD CONSTRAINT json_values_fk FOREIGN KEY (id_table) REFERENCES core.tables(id_table) ON UPDATE CASCADE ON DELETE CASCADE;
 B   ALTER TABLE ONLY core.json_values DROP CONSTRAINT json_values_fk;
       core       postgres    false    202    2792    208                       2606    185568 
   log log_fk    FK CONSTRAINT     �   ALTER TABLE ONLY core.log
    ADD CONSTRAINT log_fk FOREIGN KEY (id_table) REFERENCES core.tables(id_table) ON UPDATE CASCADE ON DELETE CASCADE;
 2   ALTER TABLE ONLY core.log DROP CONSTRAINT log_fk;
       core       postgres    false    2792    204    202                       2606    185573    log log_user_fk    FK CONSTRAINT     �   ALTER TABLE ONLY core.log
    ADD CONSTRAINT log_user_fk FOREIGN KEY (id_user) REFERENCES core.users(id_user) ON UPDATE CASCADE ON DELETE CASCADE;
 7   ALTER TABLE ONLY core.log DROP CONSTRAINT log_user_fk;
       core       postgres    false    204    2790    200                       2606    185706    role_rights role_rights_fk    FK CONSTRAINT     �   ALTER TABLE ONLY core.role_rights
    ADD CONSTRAINT role_rights_fk FOREIGN KEY (id_role) REFERENCES core.roles(id_role) ON UPDATE CASCADE ON DELETE CASCADE;
 B   ALTER TABLE ONLY core.role_rights DROP CONSTRAINT role_rights_fk;
       core       postgres    false    212    213    2804                       2606    185711    role_rights role_rights_fk_1    FK CONSTRAINT     �   ALTER TABLE ONLY core.role_rights
    ADD CONSTRAINT role_rights_fk_1 FOREIGN KEY (id_right) REFERENCES core.rights(id_right) ON UPDATE CASCADE ON DELETE CASCADE;
 D   ALTER TABLE ONLY core.role_rights DROP CONSTRAINT role_rights_fk_1;
       core       postgres    false    213    2802    210            	           2606    185721    user_roles user_roles_fk    FK CONSTRAINT     �   ALTER TABLE ONLY core.user_roles
    ADD CONSTRAINT user_roles_fk FOREIGN KEY (id_user) REFERENCES core.users(id_user) ON UPDATE CASCADE ON DELETE CASCADE;
 @   ALTER TABLE ONLY core.user_roles DROP CONSTRAINT user_roles_fk;
       core       postgres    false    214    2790    200            
           2606    185726    user_roles user_roles_fk_1    FK CONSTRAINT     �   ALTER TABLE ONLY core.user_roles
    ADD CONSTRAINT user_roles_fk_1 FOREIGN KEY (id_role) REFERENCES core.roles(id_role) ON UPDATE CASCADE ON DELETE CASCADE;
 B   ALTER TABLE ONLY core.user_roles DROP CONSTRAINT user_roles_fk_1;
       core       postgres    false    212    2804    214                       2606    194014    clients clients_fk    FK CONSTRAINT     �   ALTER TABLE ONLY licenses.clients
    ADD CONSTRAINT clients_fk FOREIGN KEY (id_group) REFERENCES licenses.client_groups(id_group) ON UPDATE CASCADE ON DELETE CASCADE;
 >   ALTER TABLE ONLY licenses.clients DROP CONSTRAINT clients_fk;
       licenses       postgres    false    2810    216    215                       2606    194041 "   license_plugins license_plugins_fk    FK CONSTRAINT     �   ALTER TABLE ONLY licenses.license_plugins
    ADD CONSTRAINT license_plugins_fk FOREIGN KEY (id_license) REFERENCES licenses.licenses(id_license) ON UPDATE CASCADE ON DELETE CASCADE;
 N   ALTER TABLE ONLY licenses.license_plugins DROP CONSTRAINT license_plugins_fk;
       licenses       postgres    false    217    2814    219                       2606    194046 $   license_plugins license_plugins_fk_1    FK CONSTRAINT     �   ALTER TABLE ONLY licenses.license_plugins
    ADD CONSTRAINT license_plugins_fk_1 FOREIGN KEY (id_plugin) REFERENCES licenses.plugins(id_plugin) ON UPDATE CASCADE ON DELETE CASCADE;
 P   ALTER TABLE ONLY licenses.license_plugins DROP CONSTRAINT license_plugins_fk_1;
       licenses       postgres    false    2816    218    219                       2606    194021    licenses licenses_fk    FK CONSTRAINT     �   ALTER TABLE ONLY licenses.licenses
    ADD CONSTRAINT licenses_fk FOREIGN KEY (id_client) REFERENCES licenses.clients(id_client) ON UPDATE CASCADE ON DELETE CASCADE;
 @   ALTER TABLE ONLY licenses.licenses DROP CONSTRAINT licenses_fk;
       licenses       postgres    false    217    216    2812            �      x������ � �      �      x������ � �      �      x������ � �      �   S   x�3���LN�+N-漰����6\�ta����^�qa����b���꿰��.΂�J��]��n��d������ '�=�      �      x�3�4�2�4����� )�      �   W   x���	�0D��*R�-�bT���q���v䆹�7�ԁ����&���ds$�{�:�RJXɮ��-4�.�&�.i+��8�      �      x������ � �      �      x�3�4�2�4����� k�      �   �   x�m�=�@��z���Ο�xE&�\�W�66z=Q0��W��F�J��&;����O35�V^�t��{�pGx���)#��2�d�V-{b��q��6}�%���x6�󒶼�9]�lp�Ud_LҰ-v���M��/BE�o#g�����B{��[��u��_r�v�      �   �   x�e���0�s\,Z�v��ˮ?uDኄDHT�r���3̜�q�����>�$�`*�-�0�w�ۢO��������~z7#�c�
x����,=�\��V}��?�S7b�}0�X@���.0:�����v�姣1�R�      �   @  x��ѱm\1���^�%���D&pCR���)d��ص��zE���U��'2��b$�A�C5&R
�j�Dy��X[*�A,���ծ��<���8����������}�����=�8x,�ȡ,&I���{憩{W�����ׂ��0b�%ZΆ@�rIL�j����f�p%�D��0������T�����ޯ1�hR4k�(�a��M�V��y�T��Vץ���`D�� m<���5��o�N��� �NM���}*	lE�A�R���֜-eE^��u���:&VL��ו�鲣+����p�5��      �      x������ � �      �      x������ � �      �      x������ � �      d    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    185383 	   licensing    DATABASE     �   CREATE DATABASE licensing WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';
    DROP DATABASE licensing;
             postgres    false                        2615    185394    core    SCHEMA        CREATE SCHEMA core;
    DROP SCHEMA core;
             postgres    false            
            2615    185384    licenses    SCHEMA        CREATE SCHEMA licenses;
    DROP SCHEMA licenses;
             postgres    false            �            1255    185735    get_user(text, text)    FUNCTION     V  CREATE FUNCTION core.get_user(_login text, _password text) RETURNS TABLE(id_user integer, login text, name text, comment text, active boolean, superuser boolean, rights text)
    LANGUAGE plpgsql
    AS $$
begin
    return query
    select
        u.id_user,
        u.login,
        u."name",
        u."comment",
        u.active,
        u.superuser, 
        (select 
            array_to_json(array_agg(r.sysname))::text
         from 
            core.user_roles ur 
            inner join core.role_rights as rr
                on rr.id_role = ur.id_role 
            inner join core.rights r
                on r.id_right = rr.id_right
         where 
            ur.id_user = u.id_user ) as rights
    from
        core.users u 
    where 
        u.login = _login 
        and (u."password" is null or md5(_password) = u."password" );
END;
$$;
 :   DROP FUNCTION core.get_user(_login text, _password text);
       core       postgres    false    8            �            1259    185468    json_params    TABLE     {   CREATE TABLE core.json_params (
    id_json_param integer NOT NULL,
    id_table integer NOT NULL,
    param_data jsonb
);
    DROP TABLE core.json_params;
       core         postgres    false    8            �           0    0    TABLE json_params    COMMENT     �   COMMENT ON TABLE core.json_params IS 'Дополнительные параметры для описания записей в таблицах';
            core       postgres    false    206            �            1259    185466    json_params_id_json_param_seq    SEQUENCE     �   CREATE SEQUENCE core.json_params_id_json_param_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE core.json_params_id_json_param_seq;
       core       postgres    false    8    206            �           0    0    json_params_id_json_param_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE core.json_params_id_json_param_seq OWNED BY core.json_params.id_json_param;
            core       postgres    false    205            �            1259    185480    json_values    TABLE     �   CREATE TABLE core.json_values (
    id_json_value integer NOT NULL,
    id_table integer NOT NULL,
    id_record bigint NOT NULL,
    value_data jsonb
);
    DROP TABLE core.json_values;
       core         postgres    false    8            �           0    0    TABLE json_values    COMMENT     �   COMMENT ON TABLE core.json_values IS 'Значения дополнительных параметров для каждой записи любой таблицы';
            core       postgres    false    208            �            1259    185478    json_values_id_json_value_seq    SEQUENCE     �   CREATE SEQUENCE core.json_values_id_json_value_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE core.json_values_id_json_value_seq;
       core       postgres    false    8    208            �           0    0    json_values_id_json_value_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE core.json_values_id_json_value_seq OWNED BY core.json_values.id_json_value;
            core       postgres    false    207            �            1259    185439    log    TABLE     �   CREATE TABLE core.log (
    id_log bigint NOT NULL,
    id_table integer,
    id_record bigint,
    event text,
    event_data jsonb,
    date_time timestamp with time zone,
    id_user integer NOT NULL
);
    DROP TABLE core.log;
       core         postgres    false    8            �           0    0 	   TABLE log    COMMENT     >   COMMENT ON TABLE core.log IS 'Журнал действий';
            core       postgres    false    204            �            1259    185437    log_id_log_seq    SEQUENCE     u   CREATE SEQUENCE core.log_id_log_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE core.log_id_log_seq;
       core       postgres    false    204    8            �           0    0    log_id_log_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE core.log_id_log_seq OWNED BY core.log.id_log;
            core       postgres    false    203            �            1259    185681    rights    TABLE     f   CREATE TABLE core.rights (
    id_right integer NOT NULL,
    sysname text NOT NULL,
    name text
);
    DROP TABLE core.rights;
       core         postgres    false    8            �           0    0    TABLE rights    COMMENT     =   COMMENT ON TABLE core.rights IS 'Права доступа';
            core       postgres    false    210            �            1259    185679    rights_id_right_seq    SEQUENCE     �   CREATE SEQUENCE core.rights_id_right_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE core.rights_id_right_seq;
       core       postgres    false    8    210            �           0    0    rights_id_right_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE core.rights_id_right_seq OWNED BY core.rights.id_right;
            core       postgres    false    209            �            1259    185701    role_rights    TABLE     _   CREATE TABLE core.role_rights (
    id_role integer NOT NULL,
    id_right integer NOT NULL
);
    DROP TABLE core.role_rights;
       core         postgres    false    8            �           0    0    TABLE role_rights    COMMENT     =   COMMENT ON TABLE core.role_rights IS 'Права роли	';
            core       postgres    false    213            �            1259    185692    roles    TABLE     R   CREATE TABLE core.roles (
    id_role integer NOT NULL,
    name text NOT NULL
);
    DROP TABLE core.roles;
       core         postgres    false    8            �           0    0    TABLE roles    COMMENT     ,   COMMENT ON TABLE core.roles IS 'Роли	';
            core       postgres    false    212            �            1259    185690    roles_id_role_seq    SEQUENCE     �   CREATE SEQUENCE core.roles_id_role_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE core.roles_id_role_seq;
       core       postgres    false    8    212            �           0    0    roles_id_role_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE core.roles_id_role_seq OWNED BY core.roles.id_role;
            core       postgres    false    211            �            1259    185428    tables    TABLE     f   CREATE TABLE core.tables (
    id_table integer NOT NULL,
    sysname text NOT NULL,
    name text
);
    DROP TABLE core.tables;
       core         postgres    false    8            �           0    0    TABLE tables    COMMENT     =   COMMENT ON TABLE core.tables IS 'Список таблиц';
            core       postgres    false    202            �            1259    185426    tables_id_table_seq    SEQUENCE     �   CREATE SEQUENCE core.tables_id_table_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE core.tables_id_table_seq;
       core       postgres    false    202    8            �           0    0    tables_id_table_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE core.tables_id_table_seq OWNED BY core.tables.id_table;
            core       postgres    false    201            �            1259    185716 
   user_roles    TABLE     ]   CREATE TABLE core.user_roles (
    id_user integer NOT NULL,
    id_role integer NOT NULL
);
    DROP TABLE core.user_roles;
       core         postgres    false    8            �           0    0    TABLE user_roles    COMMENT     I   COMMENT ON TABLE core.user_roles IS 'Роли пользователя';
            core       postgres    false    214            �            1259    185417    users    TABLE     �   CREATE TABLE core.users (
    id_user integer NOT NULL,
    login text NOT NULL,
    password text,
    name text,
    comment text,
    active boolean DEFAULT false,
    superuser boolean DEFAULT false
);
    DROP TABLE core.users;
       core         postgres    false    8            �           0    0    TABLE users    COMMENT     J   COMMENT ON TABLE core.users IS 'Список пользователей';
            core       postgres    false    200            �            1259    185415    users_id_user_seq    SEQUENCE     �   CREATE SEQUENCE core.users_id_user_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE core.users_id_user_seq;
       core       postgres    false    200    8            �           0    0    users_id_user_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE core.users_id_user_seq OWNED BY core.users.id_user;
            core       postgres    false    199            �            1259    193976    client_groups    TABLE     ~   CREATE TABLE licenses.client_groups (
    id_group uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL
);
 #   DROP TABLE licenses.client_groups;
       licenses         postgres    false    10            �            1259    193996    clients    TABLE     �   CREATE TABLE licenses.clients (
    id_client uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    id_group uuid NOT NULL,
    name text NOT NULL,
    json_data text
);
    DROP TABLE licenses.clients;
       licenses         postgres    false    10            �            1259    194035    license_plugins    TABLE     �   CREATE TABLE licenses.license_plugins (
    id_license_plugin uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    id_license uuid NOT NULL,
    id_plugin uuid NOT NULL
);
 %   DROP TABLE licenses.license_plugins;
       licenses         postgres    false    10            �            1259    194004    licenses    TABLE     �   CREATE TABLE licenses.licenses (
    id_license uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    id_client uuid NOT NULL,
    serial_key text NOT NULL,
    date_finish integer,
    active boolean DEFAULT false
);
    DROP TABLE licenses.licenses;
       licenses         postgres    false    10            �            1259    194026    plugins    TABLE     �   CREATE TABLE licenses.plugins (
    id_plugin uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    sysname text NOT NULL,
    name text NOT NULL,
    comment text
);
    DROP TABLE licenses.plugins;
       licenses         postgres    false    10            �
           2604    185471    json_params id_json_param    DEFAULT     �   ALTER TABLE ONLY core.json_params ALTER COLUMN id_json_param SET DEFAULT nextval('core.json_params_id_json_param_seq'::regclass);
 F   ALTER TABLE core.json_params ALTER COLUMN id_json_param DROP DEFAULT;
       core       postgres    false    205    206    206            �
           2604    185483    json_values id_json_value    DEFAULT     �   ALTER TABLE ONLY core.json_values ALTER COLUMN id_json_value SET DEFAULT nextval('core.json_values_id_json_value_seq'::regclass);
 F   ALTER TABLE core.json_values ALTER COLUMN id_json_value DROP DEFAULT;
       core       postgres    false    208    207    208            �
           2604    185442 
   log id_log    DEFAULT     d   ALTER TABLE ONLY core.log ALTER COLUMN id_log SET DEFAULT nextval('core.log_id_log_seq'::regclass);
 7   ALTER TABLE core.log ALTER COLUMN id_log DROP DEFAULT;
       core       postgres    false    204    203    204            �
           2604    185684    rights id_right    DEFAULT     n   ALTER TABLE ONLY core.rights ALTER COLUMN id_right SET DEFAULT nextval('core.rights_id_right_seq'::regclass);
 <   ALTER TABLE core.rights ALTER COLUMN id_right DROP DEFAULT;
       core       postgres    false    210    209    210            �
           2604    185695    roles id_role    DEFAULT     j   ALTER TABLE ONLY core.roles ALTER COLUMN id_role SET DEFAULT nextval('core.roles_id_role_seq'::regclass);
 :   ALTER TABLE core.roles ALTER COLUMN id_role DROP DEFAULT;
       core       postgres    false    212    211    212            �
           2604    185431    tables id_table    DEFAULT     n   ALTER TABLE ONLY core.tables ALTER COLUMN id_table SET DEFAULT nextval('core.tables_id_table_seq'::regclass);
 <   ALTER TABLE core.tables ALTER COLUMN id_table DROP DEFAULT;
       core       postgres    false    201    202    202            �
           2604    185420    users id_user    DEFAULT     j   ALTER TABLE ONLY core.users ALTER COLUMN id_user SET DEFAULT nextval('core.users_id_user_seq'::regclass);
 :   ALTER TABLE core.users ALTER COLUMN id_user DROP DEFAULT;
       core       postgres    false    199    200    200            �          0    185468    json_params 
   TABLE DATA               H   COPY core.json_params (id_json_param, id_table, param_data) FROM stdin;
    core       postgres    false    206   �       �          0    185480    json_values 
   TABLE DATA               S   COPY core.json_values (id_json_value, id_table, id_record, value_data) FROM stdin;
    core       postgres    false    208   �       �          0    185439    log 
   TABLE DATA               _   COPY core.log (id_log, id_table, id_record, event, event_data, date_time, id_user) FROM stdin;
    core       postgres    false    204   �       �          0    185681    rights 
   TABLE DATA               7   COPY core.rights (id_right, sysname, name) FROM stdin;
    core       postgres    false    210   �       �          0    185701    role_rights 
   TABLE DATA               6   COPY core.role_rights (id_role, id_right) FROM stdin;
    core       postgres    false    213   Y        �          0    185692    roles 
   TABLE DATA               ,   COPY core.roles (id_role, name) FROM stdin;
    core       postgres    false    212   ~        �          0    185428    tables 
   TABLE DATA               7   COPY core.tables (id_table, sysname, name) FROM stdin;
    core       postgres    false    202   �        �          0    185716 
   user_roles 
   TABLE DATA               4   COPY core.user_roles (id_user, id_role) FROM stdin;
    core       postgres    false    214          �          0    185417    users 
   TABLE DATA               Y   COPY core.users (id_user, login, password, name, comment, active, superuser) FROM stdin;
    core       postgres    false    200   '       �          0    193976    client_groups 
   TABLE DATA               9   COPY licenses.client_groups (id_group, name) FROM stdin;
    licenses       postgres    false    215   �       �          0    193996    clients 
   TABLE DATA               I   COPY licenses.clients (id_client, id_group, name, json_data) FROM stdin;
    licenses       postgres    false    216   �       �          0    194035    license_plugins 
   TABLE DATA               U   COPY licenses.license_plugins (id_license_plugin, id_license, id_plugin) FROM stdin;
    licenses       postgres    false    219   �       �          0    194004    licenses 
   TABLE DATA               \   COPY licenses.licenses (id_license, id_client, serial_key, date_finish, active) FROM stdin;
    licenses       postgres    false    217   �       �          0    194026    plugins 
   TABLE DATA               F   COPY licenses.plugins (id_plugin, sysname, name, comment) FROM stdin;
    licenses       postgres    false    218          �           0    0    json_params_id_json_param_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('core.json_params_id_json_param_seq', 1, false);
            core       postgres    false    205            �           0    0    json_values_id_json_value_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('core.json_values_id_json_value_seq', 1, false);
            core       postgres    false    207            �           0    0    log_id_log_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('core.log_id_log_seq', 1, false);
            core       postgres    false    203            �           0    0    rights_id_right_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('core.rights_id_right_seq', 4, true);
            core       postgres    false    209            �           0    0    roles_id_role_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('core.roles_id_role_seq', 4, true);
            core       postgres    false    211            �           0    0    tables_id_table_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('core.tables_id_table_seq', 4, true);
            core       postgres    false    201            �           0    0    users_id_user_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('core.users_id_user_seq', 8, true);
            core       postgres    false    199            �
           2606    185476    json_params json_params_pk 
   CONSTRAINT     a   ALTER TABLE ONLY core.json_params
    ADD CONSTRAINT json_params_pk PRIMARY KEY (id_json_param);
 B   ALTER TABLE ONLY core.json_params DROP CONSTRAINT json_params_pk;
       core         postgres    false    206            �
           2606    185488    json_values json_values_pk 
   CONSTRAINT     a   ALTER TABLE ONLY core.json_values
    ADD CONSTRAINT json_values_pk PRIMARY KEY (id_json_value);
 B   ALTER TABLE ONLY core.json_values DROP CONSTRAINT json_values_pk;
       core         postgres    false    208            �
           2606    185447 
   log log_pk 
   CONSTRAINT     J   ALTER TABLE ONLY core.log
    ADD CONSTRAINT log_pk PRIMARY KEY (id_log);
 2   ALTER TABLE ONLY core.log DROP CONSTRAINT log_pk;
       core         postgres    false    204            �
           2606    185689    rights rights_pk 
   CONSTRAINT     R   ALTER TABLE ONLY core.rights
    ADD CONSTRAINT rights_pk PRIMARY KEY (id_right);
 8   ALTER TABLE ONLY core.rights DROP CONSTRAINT rights_pk;
       core         postgres    false    210            �
           2606    185705    role_rights role_rights_pk 
   CONSTRAINT     e   ALTER TABLE ONLY core.role_rights
    ADD CONSTRAINT role_rights_pk PRIMARY KEY (id_role, id_right);
 B   ALTER TABLE ONLY core.role_rights DROP CONSTRAINT role_rights_pk;
       core         postgres    false    213    213            �
           2606    185700    roles roles_pk 
   CONSTRAINT     O   ALTER TABLE ONLY core.roles
    ADD CONSTRAINT roles_pk PRIMARY KEY (id_role);
 6   ALTER TABLE ONLY core.roles DROP CONSTRAINT roles_pk;
       core         postgres    false    212            �
           2606    185436    tables tables_pk 
   CONSTRAINT     R   ALTER TABLE ONLY core.tables
    ADD CONSTRAINT tables_pk PRIMARY KEY (id_table);
 8   ALTER TABLE ONLY core.tables DROP CONSTRAINT tables_pk;
       core         postgres    false    202            �
           2606    185720    user_roles user_roles_pk 
   CONSTRAINT     b   ALTER TABLE ONLY core.user_roles
    ADD CONSTRAINT user_roles_pk PRIMARY KEY (id_user, id_role);
 @   ALTER TABLE ONLY core.user_roles DROP CONSTRAINT user_roles_pk;
       core         postgres    false    214    214            �
           2606    185425    users users_pk 
   CONSTRAINT     O   ALTER TABLE ONLY core.users
    ADD CONSTRAINT users_pk PRIMARY KEY (id_user);
 6   ALTER TABLE ONLY core.users DROP CONSTRAINT users_pk;
       core         postgres    false    200            �
           2606    193983    client_groups client_groups_pk 
   CONSTRAINT     d   ALTER TABLE ONLY licenses.client_groups
    ADD CONSTRAINT client_groups_pk PRIMARY KEY (id_group);
 J   ALTER TABLE ONLY licenses.client_groups DROP CONSTRAINT client_groups_pk;
       licenses         postgres    false    215            �
           2606    194013    clients clients_pk 
   CONSTRAINT     Y   ALTER TABLE ONLY licenses.clients
    ADD CONSTRAINT clients_pk PRIMARY KEY (id_client);
 >   ALTER TABLE ONLY licenses.clients DROP CONSTRAINT clients_pk;
       licenses         postgres    false    216                       2606    194040 "   license_plugins license_plugins_pk 
   CONSTRAINT     q   ALTER TABLE ONLY licenses.license_plugins
    ADD CONSTRAINT license_plugins_pk PRIMARY KEY (id_license_plugin);
 N   ALTER TABLE ONLY licenses.license_plugins DROP CONSTRAINT license_plugins_pk;
       licenses         postgres    false    219            �
           2606    194020    licenses licenses_pk 
   CONSTRAINT     \   ALTER TABLE ONLY licenses.licenses
    ADD CONSTRAINT licenses_pk PRIMARY KEY (id_license);
 @   ALTER TABLE ONLY licenses.licenses DROP CONSTRAINT licenses_pk;
       licenses         postgres    false    217                        2606    194034    plugins plugins_pk 
   CONSTRAINT     Y   ALTER TABLE ONLY licenses.plugins
    ADD CONSTRAINT plugins_pk PRIMARY KEY (id_plugin);
 >   ALTER TABLE ONLY licenses.plugins DROP CONSTRAINT plugins_pk;
       licenses         postgres    false    218            �
           1259    185477    json_params_id_table_idx    INDEX     Y   CREATE UNIQUE INDEX json_params_id_table_idx ON core.json_params USING btree (id_table);
 *   DROP INDEX core.json_params_id_table_idx;
       core         postgres    false    206            �
           1259    185453    log_id_record_idx    INDEX     D   CREATE INDEX log_id_record_idx ON core.log USING btree (id_record);
 #   DROP INDEX core.log_id_record_idx;
       core         postgres    false    204                       2606    185546    json_params json_params_fk    FK CONSTRAINT     �   ALTER TABLE ONLY core.json_params
    ADD CONSTRAINT json_params_fk FOREIGN KEY (id_table) REFERENCES core.tables(id_table) ON UPDATE CASCADE ON DELETE CASCADE;
 B   ALTER TABLE ONLY core.json_params DROP CONSTRAINT json_params_fk;
       core       postgres    false    202    2792    206                       2606    185489    json_values json_values_fk    FK CONSTRAINT     �   ALTER TABLE ONLY core.json_values
    ADD CONSTRAINT json_values_fk FOREIGN KEY (id_table) REFERENCES core.tables(id_table) ON UPDATE CASCADE ON DELETE CASCADE;
 B   ALTER TABLE ONLY core.json_values DROP CONSTRAINT json_values_fk;
       core       postgres    false    202    2792    208                       2606    185568 
   log log_fk    FK CONSTRAINT     �   ALTER TABLE ONLY core.log
    ADD CONSTRAINT log_fk FOREIGN KEY (id_table) REFERENCES core.tables(id_table) ON UPDATE CASCADE ON DELETE CASCADE;
 2   ALTER TABLE ONLY core.log DROP CONSTRAINT log_fk;
       core       postgres    false    2792    204    202                       2606    185573    log log_user_fk    FK CONSTRAINT     �   ALTER TABLE ONLY core.log
    ADD CONSTRAINT log_user_fk FOREIGN KEY (id_user) REFERENCES core.users(id_user) ON UPDATE CASCADE ON DELETE CASCADE;
 7   ALTER TABLE ONLY core.log DROP CONSTRAINT log_user_fk;
       core       postgres    false    204    2790    200                       2606    185706    role_rights role_rights_fk    FK CONSTRAINT     �   ALTER TABLE ONLY core.role_rights
    ADD CONSTRAINT role_rights_fk FOREIGN KEY (id_role) REFERENCES core.roles(id_role) ON UPDATE CASCADE ON DELETE CASCADE;
 B   ALTER TABLE ONLY core.role_rights DROP CONSTRAINT role_rights_fk;
       core       postgres    false    212    213    2804                       2606    185711    role_rights role_rights_fk_1    FK CONSTRAINT     �   ALTER TABLE ONLY core.role_rights
    ADD CONSTRAINT role_rights_fk_1 FOREIGN KEY (id_right) REFERENCES core.rights(id_right) ON UPDATE CASCADE ON DELETE CASCADE;
 D   ALTER TABLE ONLY core.role_rights DROP CONSTRAINT role_rights_fk_1;
       core       postgres    false    213    2802    210            	           2606    185721    user_roles user_roles_fk    FK CONSTRAINT     �   ALTER TABLE ONLY core.user_roles
    ADD CONSTRAINT user_roles_fk FOREIGN KEY (id_user) REFERENCES core.users(id_user) ON UPDATE CASCADE ON DELETE CASCADE;
 @   ALTER TABLE ONLY core.user_roles DROP CONSTRAINT user_roles_fk;
       core       postgres    false    214    2790    200            
           2606    185726    user_roles user_roles_fk_1    FK CONSTRAINT     �   ALTER TABLE ONLY core.user_roles
    ADD CONSTRAINT user_roles_fk_1 FOREIGN KEY (id_role) REFERENCES core.roles(id_role) ON UPDATE CASCADE ON DELETE CASCADE;
 B   ALTER TABLE ONLY core.user_roles DROP CONSTRAINT user_roles_fk_1;
       core       postgres    false    212    2804    214                       2606    194014    clients clients_fk    FK CONSTRAINT     �   ALTER TABLE ONLY licenses.clients
    ADD CONSTRAINT clients_fk FOREIGN KEY (id_group) REFERENCES licenses.client_groups(id_group) ON UPDATE CASCADE ON DELETE CASCADE;
 >   ALTER TABLE ONLY licenses.clients DROP CONSTRAINT clients_fk;
       licenses       postgres    false    2810    216    215                       2606    194041 "   license_plugins license_plugins_fk    FK CONSTRAINT     �   ALTER TABLE ONLY licenses.license_plugins
    ADD CONSTRAINT license_plugins_fk FOREIGN KEY (id_license) REFERENCES licenses.licenses(id_license) ON UPDATE CASCADE ON DELETE CASCADE;
 N   ALTER TABLE ONLY licenses.license_plugins DROP CONSTRAINT license_plugins_fk;
       licenses       postgres    false    217    2814    219                       2606    194046 $   license_plugins license_plugins_fk_1    FK CONSTRAINT     �   ALTER TABLE ONLY licenses.license_plugins
    ADD CONSTRAINT license_plugins_fk_1 FOREIGN KEY (id_plugin) REFERENCES licenses.plugins(id_plugin) ON UPDATE CASCADE ON DELETE CASCADE;
 P   ALTER TABLE ONLY licenses.license_plugins DROP CONSTRAINT license_plugins_fk_1;
       licenses       postgres    false    2816    218    219                       2606    194021    licenses licenses_fk    FK CONSTRAINT     �   ALTER TABLE ONLY licenses.licenses
    ADD CONSTRAINT licenses_fk FOREIGN KEY (id_client) REFERENCES licenses.clients(id_client) ON UPDATE CASCADE ON DELETE CASCADE;
 @   ALTER TABLE ONLY licenses.licenses DROP CONSTRAINT licenses_fk;
       licenses       postgres    false    217    216    2812           