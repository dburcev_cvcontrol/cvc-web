export const environment = {
  production: true,
  intercepted: false,
  dataService: {
    server: 'http://license.cvcontrol.tech:4300/',
    authLoginURL: 'api/auth',
    authExpire: 360000, // 60 минут

    groupsURL: 'api/groups/',
    clientsURL: 'api/clients/',
    licensesURL: 'api/licenses/',
    clientPluginsURL: 'api/licenses/plugins/',
    pluginsURL: 'api/plugins/',
    paysURL: 'api/pay/',
    rightsURL: 'api/core/rights/',
    rolesURL: 'api/core/roles/',
    usersURL: 'api/core/users/'
  },
  serialService: {
    server: 'https://license.cvcontrol.tech/',
    serialURL: 'api/license/',
    getserialURL: 'api/getlicense',

    login: 'admin',
    password: 'Qwerty_1',

    statuses_created: 1,
    statuses_active: 2,
    statuses_blocked: 3,
    statuses_stopped: 4,
    statuses_deleted: 5
  },
  rights: {
    superuser: 'superuser', // Администратор системы
    licenses: 'licenses',   // Право на лицензирование клиентов
    pay: 'pay'              // Право на работу с оплатой
  }
};
