// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  intercepted: false,
  dataService: {
    server: 'http://localhost:4300/',
    // server: 'http://license.cvcontrol.tech:4300/',
    authLoginURL: 'api/auth',
    authExpire: 360000, // 60 минут

    groupsURL: 'api/groups/',
    clientsURL: 'api/clients/',
    licensesURL: 'api/licenses/',
    licensesAllURL: 'api/licenses/all/',
    clientPluginsURL: 'api/licenses/plugins/',
    pluginsURL: 'api/plugins/',
    paysURL: 'api/pay/',
    rightsURL: 'api/core/rights/',
    rolesURL: 'api/core/roles/',
    usersURL: 'api/core/users/'
  },
  serialService: {
    server: 'https://license.cvcontrol.tech/',
    serialURL: 'api/license',
    getserialURL: 'api/getlicense',

    login: 'admin',
    password: 'Qwerty_1',

    statuses_created: 1,
    statuses_active: 2,
    statuses_blocked: 3,
    statuses_stopped: 4,
    statuses_deleted: 5
  },
  rights: {
    superuser: 'superuser', // Администратор системы
    licenses: 'licenses',   // Право на лицензирование клиентов
    pay: 'pay'              // Право на работу с оплатой
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
