import { Component, OnInit } from '@angular/core';
import {CoreService} from '../../shared/services/core.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.scss']
})
export class UsersPageComponent implements OnInit {

  constructor(
    public coresrvc: CoreService
  ) { }

  ngOnInit(): void {
    this.coresrvc.getUsers().subscribe(() => {});
    this.coresrvc.getRoles().subscribe(() => {});
  }

  onRemoveUserClick(idUser: number) {
    this.coresrvc.removeUser(idUser).subscribe(() => {});
  }
}
