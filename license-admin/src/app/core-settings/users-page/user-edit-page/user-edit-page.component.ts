import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {CoreService} from '../../../shared/services/core.service';
import {take} from 'rxjs/operators';
import {User} from '../../../shared/interfaces';

@Component({
  selector: 'app-user-edit-page',
  templateUrl: './user-edit-page.component.html',
  styleUrls: ['./user-edit-page.component.scss']
})
export class UserEditPageComponent implements OnInit {

  public editItem: User = {
    login: '',
    name: '',
    comment: '',
    active: false,
    superuser: false,
    roles: []
  };
  submited = false;
  form: FormGroup;

  constructor(private route: ActivatedRoute,
              public coresrv: CoreService,
              private router: Router) { }

  ngOnInit(): void {
    this.coresrv.getRoles().pipe(take(1)).subscribe(() => {});
    this.route.params.pipe(take(1)).subscribe((params: Params) => {
      if (params.id === '0') {
        this.form = new FormGroup({
          login: new FormControl('', Validators.required),
          name: new FormControl('', Validators.required),
          comment: new FormControl(''),
          active: new FormControl(false),
          superuser: new FormControl(false),
        });
        this.coresrv.roles.forEach(role => {
          this.form.addControl('role_' + role.id_role,
            new FormControl(false));
        });
      } else {
        this.coresrv.getUserDetail(params.id).pipe(take(1)).subscribe(item => {
          this.editItem = item;

          if (this.editItem) {
            this.form = new FormGroup({
              login: new FormControl(this.editItem.login, Validators.required),
              name: new FormControl(this.editItem.name, Validators.required),
              comment: new FormControl(this.editItem.comment),
              active: new FormControl(this.editItem.active),
              superuser: new FormControl(this.editItem.superuser),
            });
          } else {
            this.form = new FormGroup({
              login: new FormControl('', Validators.required),
              name: new FormControl('', Validators.required),
              comment: new FormControl(''),
              active: new FormControl(false),
              superuser: new FormControl(false),
            });
          }
          this.coresrv.roles.forEach(role => {
            this.form.addControl('role_' + role.id_role,
              new FormControl(this.roleContainUser(item.roles, role.id_role)));
          });
        });
      }
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }
    this.submited = true;

    this.editItem.login     = this.form.get('login').value;
    this.editItem.name      = this.form.get('name').value;
    this.editItem.comment   = this.form.get('comment').value;
    this.editItem.active    = this.form.get('active').value;
    this.editItem.superuser = this.form.get('superuser').value;

    const roles = [];
    this.coresrv.roles.forEach(role => {
      if (this.form.get('role_' + role.id_role).value) {
        roles.push(role);
      }
    });
    this.editItem.roles = roles;

    this.coresrv.addOrUpdateUser(this.editItem).subscribe( () => {
      this.submited = false;
      this.router.navigate(['/users']);
    });

  }

  roleContainUser(roles: any, idRole: number): boolean {
    const findItem = roles.findIndex(item => item.id_role === idRole);
    return findItem !== -1;
  }
}
