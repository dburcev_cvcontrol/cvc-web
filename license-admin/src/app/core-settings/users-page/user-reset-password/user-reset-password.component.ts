import { Component, OnInit } from '@angular/core';
import {CoreService} from '../../../shared/services/core.service';
import {take} from 'rxjs/operators';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../../../shared/interfaces';

@Component({
  selector: 'app-user-reset-password',
  templateUrl: './user-reset-password.component.html',
  styleUrls: ['./user-reset-password.component.scss']
})
export class UserResetPasswordComponent implements OnInit {

  form: FormGroup;
  user: User;

  constructor(
    private route: ActivatedRoute,
    public coresrv: CoreService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.params.pipe(take(1)).subscribe((params: Params) => {
      if (params.id === '0') {
        this.router.navigate(['/users']);
      } else {
        this.coresrv.getUserDetail(params.id).pipe(take(1)).subscribe(user => {
          if (user) {
            this.user = user;
            this.form = new FormGroup({
              newPassword: new FormControl('', Validators.required),
              confirmPassword: new FormControl('', Validators.required)
            });
          } else {
            this.router.navigate(['/users']);
          }
        });
      }
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    const userReset = {
      id_user: this.user.id_user,
      password: this.form.get('newPassword').value
    };

    this.coresrv.resetPassword(userReset).pipe(take(1)).subscribe(() => {
      this.router.navigate(['/users']);
    });
  }

  formValided() {
    return this.form.valid &&
      (this.form.get('newPassword').value === this.form.get('confirmPassword').value);
  }
}
