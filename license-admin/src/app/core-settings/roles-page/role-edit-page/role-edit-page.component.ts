import { Component, OnInit } from '@angular/core';
import { Role} from '../../../shared/core.interfaces';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {CoreService} from '../../../shared/services/core.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-role-edit-page',
  templateUrl: './role-edit-page.component.html',
  styleUrls: ['./role-edit-page.component.scss']
})
export class RoleEditPageComponent implements OnInit {


  public editItem: Role = {
    name: '',
    rights: []
  };
  submited = false;
  form: FormGroup;

  constructor(private route: ActivatedRoute,
              public coresrv: CoreService,
              private router: Router) { }

  ngOnInit(): void {
    this.route.params.pipe(take(1)).subscribe((params: Params) => {
      if (params.id === '0') {
        this.form = new FormGroup({
          name: new FormControl('', Validators.required),
        });
        this.coresrv.rights.forEach(right => {
          this.form.addControl('right_' + right.sysname,
            new FormControl(false));
        });
      } else {
        this.coresrv.getRoleDetail(params.id).pipe(take(1)).subscribe(item => {
          this.editItem = item;

          if (this.editItem) {
            this.form = new FormGroup({
              name: new FormControl(this.editItem.name, Validators.required),
            });
          } else {
            this.form = new FormGroup({
              name: new FormControl('', Validators.required),
            });
          }
          this.coresrv.rights.forEach(right => {
            this.form.addControl('right_' + right.sysname,
              new FormControl(this.rightContainRole(item.rights, right.id_right)));
          });
        });
      }
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }
    this.submited = true;

    this.editItem.name    = this.form.get('name').value;

    const rights = [];
    this.coresrv.rights.forEach(right => {
      if (this.form.get('right_' + right.sysname).value) {
        rights.push(right);
      }
    });
    this.editItem.rights = rights;

    this.coresrv.addOrUpdateRole(this.editItem).subscribe( () => {
      this.submited = false;
      this.router.navigate(['/roles']);
    });

  }

  rightContainRole(rights: any, idRight: number): boolean {
    const findItem = rights.findIndex(item => item.id_right === idRight);
    return findItem !== -1;
  }
}
