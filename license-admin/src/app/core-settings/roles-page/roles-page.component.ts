import { Component, OnInit } from '@angular/core';
import {CoreService} from '../../shared/services/core.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-roles-page',
  templateUrl: './roles-page.component.html',
  styleUrls: ['./roles-page.component.scss']
})
export class RolesPageComponent implements OnInit {

  constructor(
    public coresrvc: CoreService
  ) { }

  ngOnInit(): void {
    this.coresrvc.getRoles().pipe(take(1)).subscribe(roles => {});
    this.coresrvc.getRights().pipe(take(1)).subscribe(() => {});
  }

  onRemoveRoleClick(idRole: number) {
    this.coresrvc.removeRole(idRole).pipe(take(1)).subscribe(() => {});
  }

}
