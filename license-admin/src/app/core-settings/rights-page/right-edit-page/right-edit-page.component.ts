import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {take} from 'rxjs/operators';
import {Right} from '../../../shared/core.interfaces';
import {CoreService} from '../../../shared/services/core.service';

@Component({
  selector: 'app-right-edit-page',
  templateUrl: './right-edit-page.component.html',
  styleUrls: ['./right-edit-page.component.scss']
})
export class RightEditPageComponent implements OnInit {

  editItem: Right = {
    sysname: '',
    name: ''
  };
  submited = false;
  form: FormGroup;

  constructor(private route: ActivatedRoute,
              private coresrv: CoreService,
              private router: Router) { }

  ngOnInit(): void {
    this.route.params.pipe(take(1)).subscribe((params: Params) => {
      if (params.id === '0') {
        this.form = new FormGroup({
          sysname: new FormControl('', Validators.required),
          name: new FormControl('', Validators.required)
        });
      } else {
        this.coresrv.getRightDetail(params.id).pipe(take(1)).subscribe(item => {
          this.editItem = item;

          if (this.editItem) {
            this.form = new FormGroup({
              sysname: new FormControl(this.editItem.sysname, Validators.required),
              name: new FormControl(this.editItem.name, Validators.required)
            });
          } else {
            this.form = new FormGroup({
              sysname: new FormControl('', Validators.required),
              name: new FormControl('', Validators.required)
            });
          }
        });
      }
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }
    this.submited = true;

    this.editItem.sysname = this.form.get('sysname').value;
    this.editItem.name    = this.form.get('name').value;

    this.coresrv.addOrUpdateRight(this.editItem).subscribe( () => {
      this.submited = false;
      this.router.navigate(['/rights']);
    });

  }

}
