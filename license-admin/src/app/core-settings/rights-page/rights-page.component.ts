import { Component, OnInit } from '@angular/core';
import {CoreService} from '../../shared/services/core.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-rights-page',
  templateUrl: './rights-page.component.html',
  styleUrls: ['./rights-page.component.scss']
})
export class RightsPageComponent implements OnInit {

  constructor(
    public coresrvc: CoreService
  ) { }

  ngOnInit(): void {
    this.coresrvc.getRights().pipe(take(1)).subscribe(rights => { });
  }

  onRemoveRightClick(idRight: number) {
    this.coresrvc.removeRight(idRight).pipe(take(1)).subscribe(() => {});
  }
}
