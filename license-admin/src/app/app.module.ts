import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import { AuthorizedLayoutComponent } from './shared/components/authorized-layout/authorized-layout.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { ClientPageComponent } from './client-page/client-page.component';
import { PayPageComponent } from './pay-page/pay-page.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthService} from './shared/services/auth.service';
import {SharedModule} from './shared/shared.module';
import {AuthGuard} from './shared/services/auth.guard';
import {environment} from '../environments/environment';
import { ClientListComponent } from './shared/components/client-list/client-list.component';
import {ClientService} from './shared/services/client.service';
import { ClientInfoViewComponent } from './dashboard-page/client-info-view/client-info-view.component';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import { KeyPageComponent } from './key-page/key-page.component';
import { ClientGroupsComponent } from './client-groups/client-groups.component';
import { PluginsComponent } from './plugins/plugins.component';
import { PluginPageComponent } from './plugins/plugin-page/plugin-page.component';
import { ClientGroupPageComponent } from './client-groups/client-group-page/client-group-page.component';
import {SerialService} from './shared/services/serial.service';
import {BasicAuthInterceptorInterceptor} from './shared/basic-auth-interceptor.interceptor';
import {HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { PayViewComponent } from './pay-page/pay-view/pay-view.component';
import { PayEditPageComponent } from './pay-page/pay-edit-page/pay-edit-page.component';
import { RightsPageComponent } from './core-settings/rights-page/rights-page.component';
import { RightEditPageComponent } from './core-settings/rights-page/right-edit-page/right-edit-page.component';
import { RolesPageComponent } from './core-settings/roles-page/roles-page.component';
import { RoleEditPageComponent } from './core-settings/roles-page/role-edit-page/role-edit-page.component';
import { UsersPageComponent } from './core-settings/users-page/users-page.component';
import { UserEditPageComponent } from './core-settings/users-page/user-edit-page/user-edit-page.component';
import { UserResetPasswordComponent } from './core-settings/users-page/user-reset-password/user-reset-password.component';
import { SerialDownloadPageComponent } from './dashboard-page/serial-download-page/serial-download-page.component';
import { KeySyncPageComponent } from './key-sync-page/key-sync-page.component';

const routes: Routes = [
  {path: '', component: AuthorizedLayoutComponent, children: [
    {path: '', redirectTo: '/login', pathMatch: 'full'},
    {path: 'login', component: LoginPageComponent},
    {path: 'dashboard', component: DashboardPageComponent, canActivate: [AuthGuard],
      data: {rights: [environment.rights.licenses]}},
    {path: 'serialdownload/:id', component: SerialDownloadPageComponent, canActivate: [AuthGuard],
      data: {rights: [environment.rights.licenses]}},
    {path: 'pays', component: PayPageComponent, canActivate: [AuthGuard],
      data: {rights: [environment.rights.pay]}},
    {path: 'pay/:id', component: PayEditPageComponent, canActivate: [AuthGuard],
      data: {rights: [environment.rights.pay]}},
    {path: 'key-sync', component: KeySyncPageComponent, canActivate: [AuthGuard],
      data: {rights: [environment.rights.licenses]}},
    {path: 'client/:id', component: ClientPageComponent, canActivate: [AuthGuard],
      data: {rights: [environment.rights.licenses]}},
    {path: 'key/:id', component: KeyPageComponent, canActivate: [AuthGuard],
      data: {rights: [environment.rights.licenses]}},
    {path: 'client-groups', component: ClientGroupsComponent, canActivate: [AuthGuard],
      data: {rights: [environment.rights.superuser]}},
    {path: 'client-group/:id', component: ClientGroupPageComponent, canActivate: [AuthGuard],
      data: {rights: [environment.rights.superuser]}},
    {path: 'plugins', component: PluginsComponent, canActivate: [AuthGuard],
      data: {rights: [environment.rights.superuser]}},
    {path: 'plugin/:id', component: PluginPageComponent, canActivate: [AuthGuard],
      data: {rights: [environment.rights.superuser]}},
    {path: 'rights', component: RightsPageComponent, canActivate: [AuthGuard],
      data: {rights: [environment.rights.superuser]}},
    {path: 'right/:id', component: RightEditPageComponent, canActivate: [AuthGuard],
      data: {rights: [environment.rights.superuser]}},
    {path: 'roles', component: RolesPageComponent, canActivate: [AuthGuard],
      data: {rights: [environment.rights.superuser]}},
    {path: 'role/:id', component: RoleEditPageComponent, canActivate: [AuthGuard],
      data: {rights: [environment.rights.superuser]}},
    {path: 'users', component: UsersPageComponent, canActivate: [AuthGuard],
      data: {rights: [environment.rights.superuser]}},
    {path: 'user/:id', component: UserEditPageComponent, canActivate: [AuthGuard],
      data: {rights: [environment.rights.superuser]}},
    {path: 'resetpassword/:id', component: UserResetPasswordComponent, canActivate: [AuthGuard],
      data: {rights: [environment.rights.superuser]}},
  ]}
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    SharedModule,
    HttpClientModule,
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules
    })
  ],
  exports: [
    RouterModule
  ],
  providers: [
    AuthService,
    ClientService,
    SerialService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BasicAuthInterceptorInterceptor,
      multi: true
    }
  ],
  declarations: [
    AppComponent,
    AuthorizedLayoutComponent,
    LoginPageComponent,
    DashboardPageComponent,
    ClientPageComponent,
    PayPageComponent,
    ClientListComponent,
    ClientInfoViewComponent,
    KeyPageComponent,
    ClientGroupsComponent,
    PluginsComponent,
    PluginPageComponent,
    ClientGroupPageComponent,
    PayViewComponent,
    PayEditPageComponent,
    RightsPageComponent,
    RightEditPageComponent,
    RolesPageComponent,
    RoleEditPageComponent,
    UsersPageComponent,
    UserEditPageComponent,
    UserResetPasswordComponent,
    SerialDownloadPageComponent,
    KeySyncPageComponent
  ],
  bootstrap: [AppComponent]

})

export class AppModule {}
