import { Component, OnInit } from '@angular/core';
import {ClientGroup} from '../../shared/interfaces';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ClientService} from '../../shared/services/client.service';
import {take} from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-client-group-page',
  templateUrl: './client-group-page.component.html',
  styleUrls: ['./client-group-page.component.scss']
})
export class ClientGroupPageComponent implements OnInit {

  isNew = true;
  group: ClientGroup = {
    id_group: uuidv4(),
    name: 'Новая группа'
  };
  submited = false;
  form: FormGroup;

  constructor(private route: ActivatedRoute,
              private clnt: ClientService,
              private router: Router) { }

  ngOnInit(): void {
    this.route.params.pipe(take(1)).subscribe((params: Params) => {
      if (params.id === '0') {
        this.form = new FormGroup({
          name: new FormControl('Новая группа', Validators.required),
        });
      } else {
        this.clnt.getGroupDetail(params.id).pipe(take(1)).subscribe((group) => {
          this.group = group;

          if (this.group) {
            this.isNew = false;
            this.form = new FormGroup({
              name: new FormControl(this.group.name, Validators.required)
            });
          } else {
            this.form = new FormGroup({
              name: new FormControl('Новая группа', Validators.required)
            });
          }
        });
      }
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }
    this.submited = true;

    this.group.name    = this.form.get('name').value;

    if (!this.isNew) {
      this.clnt.updateGroup(this.group).subscribe( () => {
        this.submited = false;
        this.router.navigate(['/client-groups']);
      });
    } else {
      this.clnt.addGroup(this.group).subscribe( () => {
        this.clnt.groups.push(this.group);
        this.submited = false;
        this.router.navigate(['/client-groups']);
      });
    }
  }

}
