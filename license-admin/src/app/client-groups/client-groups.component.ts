import { Component, OnInit } from '@angular/core';
import {ClientService} from '../shared/services/client.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-client-groups',
  templateUrl: './client-groups.component.html',
  styleUrls: ['./client-groups.component.scss']
})
export class ClientGroupsComponent implements OnInit {

  constructor(public clnt: ClientService) { }

  ngOnInit(): void {
    this.clnt.getGroups().pipe(take(1)).subscribe(groups => {
      this.clnt.groups = groups;
    });
  }

  onRemoveGroupClick(idGroup: any) {
    this.clnt.removeGroup(idGroup).pipe(take(1)).subscribe(() => {
      this.clnt.groups = this.clnt.groups.filter((group) => group.id_group !== idGroup);
    });
  }

}
