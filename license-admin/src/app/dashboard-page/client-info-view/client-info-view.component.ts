import {Component, OnInit} from '@angular/core';
import {ClientService} from '../../shared/services/client.service';
import {take} from 'rxjs/operators';
import {ClientLicense} from '../../shared/interfaces';
import {SerialService} from '../../shared/services/serial.service';

@Component({
  selector: 'app-client-info-view',
  templateUrl: './client-info-view.component.html',
  styleUrls: ['./client-info-view.component.scss'],
})
export class ClientInfoViewComponent implements OnInit {

  currentDate = new Date().getTime();

  constructor(
    public clnt: ClientService,
    private serial: SerialService
  ) {}

  ngOnInit(): void {
    if (!this.clnt.lastSelectedClient) {
      if (localStorage.getItem('last-client-id')) {
        this.clnt.getClientDetail(localStorage.getItem('last-client-id')).pipe(take(1)).subscribe(client => {
          this.clnt.lastSelected = client;
        });
      }
    }
  }

  onRemoveClick() {
    this.clnt.removeClient(this.clnt.lastSelectedClientId).pipe(take(1)).subscribe(() => {
      this.clnt.clients = this.clnt.clients.filter((client) => client.id_client !== this.clnt.lastSelectedClientId);
      this.clnt.lastSelected = null;
    });
  }

  onRemoveKeyClick(idLicense: any) {
    this.clnt.getLicenseDetail(idLicense).pipe(take(1)).subscribe((license: ClientLicense) => {
      if (license) {
        this.serial.removeSerialKey(license.serial_key).pipe(take(1)).subscribe(response => {
          this.clnt.removeLicense(idLicense).pipe(take(1)).subscribe(() => {
            this.clnt.licenses = this.clnt.licenses.filter(lic => lic.id_license !== idLicense);
          });
        });
      }
    });

  }
}
