import { Component, OnInit } from '@angular/core';
import {AuthService} from '../shared/services/auth.service';
import {Client} from '../shared/interfaces';
import {ClientService} from '../shared/services/client.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit {

  client: Client;

  constructor(
    private auth: AuthService,
    private clnt: ClientService
  ) { }

  ngOnInit(): void {
    localStorage.setItem('last-menu', '/dashboard');

    if (this.clnt.lastSelectedClientId) {
      this.clnt.getClientDetail(this.clnt.lastSelectedClientId).pipe(take(1)).subscribe(client => {
        this.setClientView(client);
      });
    }
  }

  setClientView(client: Client) {
    if (client) {
      this.client = client;
    }
  }

}
