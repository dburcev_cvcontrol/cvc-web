import { Component, OnInit } from '@angular/core';
import {ClientLicense, SerialDownloadInfo} from '../../shared/interfaces';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {SerialService} from '../../shared/services/serial.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {take} from 'rxjs/operators';
import {ClientService} from '../../shared/services/client.service';
import {saveAs} from 'file-saver/dist/FileSaver';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-serial-download-page',
  templateUrl: './serial-download-page.component.html',
  styleUrls: ['./serial-download-page.component.scss']
})
export class SerialDownloadPageComponent implements OnInit {

  license: ClientLicense;
  sendFile: string;
  form: FormGroup;
  error$: Subject<string> = new Subject<string>();

  constructor(
    private route: ActivatedRoute,
    private licsrv: ClientService,
    private serialsrv: SerialService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      file: new FormControl('', Validators.required),
    });
    this.route.params.pipe(take(1)).subscribe((params: Params) => {
      if (params.id === '0') {
        this.router.navigate(['/dashboard']);
      } else {
        this.licsrv.getLicenseDetail(params.id).pipe(take(1)).subscribe(license => {
          if (license) {
            this.license = license;
          } else {
            this.router.navigate(['/dashboard']);
          }
        });
      }
    });
  }

  onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        if (typeof reader.result === 'string') {
          this.sendFile = reader.result.split(',')[1];
        }
      };
    }
  }

  public base64ToBlob(b64Data, contentType= '', sliceSize= 512) {
    b64Data = b64Data.replace(/\s/g, ''); // IE compatibility...
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    return new Blob(byteArrays, {type: contentType});
  }

  submit() {
    if (this.form.invalid || !this.sendFile) {
      return;
    }

    const sendData: SerialDownloadInfo = {
      HID: this.sendFile,
      SerialNumber: this.license.serial_key,
      ClientID: this.license.id_client
    };

    this.serialsrv.getSerialKey(sendData).pipe(take(1)).subscribe(result => {
      if (result.License) {
        const blob = this.base64ToBlob(result.License, 'text/plain');
        saveAs(blob, 'license.dat');

        const serial = new Blob([result.SerialNumber], {type: 'text/plain'});
        saveAs(serial, 'serialKey.txt');
        this.router.navigate(['/dashboard']);
      } else {
        this.error$.next('Не могу получить файл лицензии');
      }
    }, error => {
      this.error$.next(error.error);
    });
  }

}
