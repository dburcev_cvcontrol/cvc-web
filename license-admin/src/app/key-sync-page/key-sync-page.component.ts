import {Component, OnInit} from '@angular/core';
import {ClientService} from '../shared/services/client.service';
import {SerialService} from '../shared/services/serial.service';
import {Client, ClientLicense} from '../shared/interfaces';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-key-sync-page',
  templateUrl: './key-sync-page.component.html',
  styleUrls: ['./key-sync-page.component.scss']
})
export class KeySyncPageComponent implements OnInit {

  keysEmpty = [];
  keysFill = [];
  clients = [];
  licenses = [];
  groups;
  groupTarget = null;

  constructor(
    private serial: SerialService,
    public client: ClientService
  ) {
    this.groups = client.getGroups();
    console.log(this.groups.length);
  }

  ngOnInit(): void {
    console.log(this.groups.length);
    if (this.client.groups && this.client.groups.length > 0) {
      this.groupTarget = this.client.groups[0].id_group;
    }

    this.client.getClients('all').subscribe(clients => {
      this.clients = clients;
    });

    this.client.getLicenses('all').subscribe(result => {
      this.licenses = result;
    });

    this.serial.getSerialsKeys().subscribe(result => {
      this.keysEmpty = [];
      this.keysFill = [];
      result.forEach( item => {
        const findClient = this.getClientBySerialKey(item.SerialNumber);
        if (findClient) {
          this.keysFill.push({
            serialKey: item.SerialNumber,
            client: findClient
          });
        } else {
          this.keysEmpty.push({
            checked: false,
            serialKey: item.SerialNumber
          });
        }
      });
    });
  }

  getClientBySerialKey(key: string): Client {
    let result = null;
    const foundKey = this.licenses.find(item => item.serial_key === key);
    if (foundKey) {
      const foundClient = this.clients.find(item => item.id_client === foundKey.id_client);
      if (foundClient) {
        result = foundClient;
      }
    }
    return result;
  }

  titleCheckClick(event: any) {
    this.keysEmpty.forEach( item => {
      item.checked = event.target.checked;
    });
    this.keysEmpty = [].concat(this.keysEmpty);
  }

  checkClick(item: any) {
    item.checked = !item.checked;
    this.keysEmpty = [].concat(this.keysEmpty);
  }

  onGroupChange(event: any) {
    this.groupTarget = event.target.value;
  }

  onAddClick() {
    if (!this.groupTarget) {
      return;
    }

    this.keysEmpty.forEach(item => {
      if (item.checked) {

        const newClient: Client = {
          id_client: uuidv4(),
          name: item.serialKey,
          id_group: this.groupTarget
        };

        this.client.addClient(newClient).subscribe( () => {

          const newLicense: ClientLicense = {
            id_license: uuidv4(),
            id_client: newClient.id_client,
            serial_key: item.serialKey,
            date_finish: null,
            active: true,
            plugins: []
          };

          this.client.addLicense(newLicense).subscribe(() => {
            this.keysFill.push({
              serialKey: item.serialKey,
              client: newClient
            });
            this.keysEmpty =this.keysEmpty.filter(subItem => subItem.serialKey !== item.serialKey);
          });
        });

      }
    });
  }
}
