import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ClientService} from '../../shared/services/client.service';
import {take} from 'rxjs/operators';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ClientPlugin} from '../../shared/interfaces';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-plugin-page',
  templateUrl: './plugin-page.component.html',
  styleUrls: ['./plugin-page.component.scss']
})
export class PluginPageComponent implements OnInit {

  isNew = true;
  plugin: ClientPlugin = {
    id_plugin: uuidv4(),
    sysname: '',
    name: '',
    comment: ''
  };
  submited = false;
  form: FormGroup;

  constructor(private route: ActivatedRoute,
              private clnt: ClientService,
              private router: Router) { }

  ngOnInit(): void {
    this.route.params.pipe(take(1)).subscribe((params: Params) => {
      if (params.id === '0') {
        this.form = new FormGroup({
          sysname: new FormControl('', Validators.required),
          name: new FormControl('Новый модуль', Validators.required),
          comment: new FormControl('')
        });
      } else {
        this.clnt.getPluginDetail(params.id).pipe(take(1)).subscribe((plugin) => {
          this.plugin = plugin;

          if (this.plugin) {
            this.isNew = false;
            this.form = new FormGroup({
              sysname: new FormControl(this.plugin.sysname, Validators.required),
              name: new FormControl(this.plugin.name, Validators.required),
              comment: new FormControl(this.plugin.comment),
            });
          } else {
            this.form = new FormGroup({
              sysname: new FormControl('', Validators.required),
              name: new FormControl('Новый модуль', Validators.required),
              comment: new FormControl('')
            });
          }
        });
      }
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }
    this.submited = true;

    this.plugin.sysname = this.form.get('sysname').value;
    this.plugin.name    = this.form.get('name').value;
    this.plugin.comment = this.form.get('comment').value;

    if (!this.isNew) {
      this.clnt.updatePlugin(this.plugin).subscribe( () => {
        this.submited = false;
        this.router.navigate(['/plugins']);
      });
    } else {
      this.clnt.addPlugin(this.plugin).subscribe( () => {
        this.clnt.plugins.push(this.plugin);
        this.submited = false;
        this.router.navigate(['/plugins']);
      });
    }
  }

}
