import { Component, OnInit } from '@angular/core';
import {ClientService} from '../shared/services/client.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-plugins',
  templateUrl: './plugins.component.html',
  styleUrls: ['./plugins.component.scss']
})
export class PluginsComponent implements OnInit {

  constructor(public clnt: ClientService) { }

  ngOnInit(): void {
    this.clnt.getPlugins().pipe(take(1)).subscribe(plugins => {
      this.clnt.plugins = plugins;
    });
  }

  onRemovePluginClick(idPlugin: any) {
    this.clnt.removePlugin(idPlugin).pipe(take(1)).subscribe(() => {
      this.clnt.plugins = this.clnt.plugins.filter((plugin) => plugin.id_plugin !== idPlugin);
    });
  }
}
