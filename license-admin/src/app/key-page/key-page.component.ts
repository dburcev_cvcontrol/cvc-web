import { Component, OnInit } from '@angular/core';
import {ClientLicense, ClientPlugin} from '../shared/interfaces';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ClientService} from '../shared/services/client.service';
import {take} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {SerialService} from '../shared/services/serial.service';
import {Subject} from 'rxjs';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-key-page',
  templateUrl: './key-page.component.html',
  styleUrls: ['./key-page.component.scss']
})
export class KeyPageComponent implements OnInit {

  public error$: Subject<string> = new Subject<string>();

  isNew = true;
  license: ClientLicense = {
    id_license: uuidv4(),
    id_client: this.clnt.lastSelectedClientId,
    serial_key: '',
    date_finish: null,
    active: false
  };
  form: FormGroup;
  submited = false;
  plugins: ClientPlugin[] = [];

  constructor(private route: ActivatedRoute,
              public clnt: ClientService,
              private serialSrv: SerialService,
              private router: Router) { }

  ngOnInit(): void {
    this.clnt.getPlugins().pipe(take(1)).subscribe(plugins => {
      this.plugins = plugins;

      this.route.params.pipe(take(1)).subscribe((params: Params) => {
        if (params.id === '0') {
          this.form = new FormGroup({
            serialKey: new FormControl('Новый серийный ключ', Validators.required),
            dateFinish: new FormControl(null),
            isActive: new FormControl(false)
          });
          this.plugins.forEach(plugin => {
            this.form.addControl('plugin_' + plugin.sysname,
              new FormControl(this.licenseContainPlugin(plugin.id_plugin)));
          });
        } else {
          this.clnt.getLicenseDetail(params.id).pipe(take(1)).subscribe((license: ClientLicense) => {
            this.license = license;
            if (license) {
              this.isNew = false;
              this.form = new FormGroup({
                serialKey: new FormControl(license.serial_key, Validators.required),
                dateFinish: new FormControl(new Date(license.date_finish * 1000)),
                isActive: new FormControl(license.active)
              });

              if (this.license.date_finish) {
                const currentDate = new Date(this.license.date_finish * 1000).toISOString().substring(0, 10);
                this.form.controls.dateFinish.setValue(currentDate);
              }
            } else {
              this.form = new FormGroup({
                serialKey: new FormControl('Новый серийный ключ', Validators.required),
                dateFinish: new FormControl(null),
                isActive: new FormControl(false)
              });
            }
            this.plugins.forEach(plugin => {
              this.form.addControl('plugin_' + plugin.sysname,
                new FormControl(this.licenseContainPlugin(plugin.id_plugin)));
            });
          });
        }
      });
    });
  }

  licenseContainPlugin(idPlugin: any): boolean {
    let res = false;
    if ((this.license) && (this.license.plugins)) {
      this.license.plugins.forEach(plugin => {
        if (plugin.id_plugin === idPlugin) {
          res = true;
        }
      });
    }
    return res;
  }

  save_db(serialServiceKey: string) {
    const plugins = [];
    this.plugins.forEach(plugin => {
      if (this.form.controls['plugin_' + plugin.sysname].value) {
        plugins.push(plugin.id_plugin.toString());
      }
    });

    const newLicense: ClientLicense = {
      id_license: this.isNew ? uuidv4() : this.license.id_license,
      id_client: !this.isNew ? this.license.id_client : this.clnt.lastSelectedClientId,
      serial_key: serialServiceKey,
      date_finish: Math.floor(new Date(this.form.get('dateFinish').value).getTime() / 1000),
      active: this.form.get('isActive').value,
      plugins
    };

    if (!this.isNew) {
      console.log('Обновление лицензии ' + newLicense.serial_key);
      this.clnt.updateLicense(newLicense).pipe(take(1)).subscribe(() => {
        console.log(newLicense);
        this.submited = false;
        this.router.navigate(['/dashboard']);
      }, error => {
        this.error$.next('Ошибка сервиса работы с БД: ' + error.message);
        this.submited = false;
      });
    } else {
      console.log('Добавление лицензии ' + serialServiceKey);
      this.clnt.addLicense(newLicense).pipe(take(1)).subscribe(() => {
        this.submited = false;
        this.router.navigate(['/dashboard']);
      }, error => {
        console.log(error.message);
        this.error$.next('Ошибка сервиса работы с БД: ' + error.message);
        this.submited = false;
      });
    }
  }

  submit() {
    if (this.form.invalid) {
      return;
    }
    this.submited = true;

    if (this.form.get('isActive').value &&
      Math.floor(new Date(this.form.get('dateFinish').value).getTime() / 1000) > 0 &&
      Math.floor(new Date(this.form.get('dateFinish').value).getTime() / 1000) <
      Math.floor(new Date().getTime() / 1000)) {
      console.log(this.form.get('dateFinish').value);
      this.form.get('isActive').setValue(false);
    }

    const serialServiceKey = {
      SerialNumber: this.isNew ? '' : this.license.serial_key,
      license: {
        LastLoadLicenseTime: 0,
        LastGenerateLicenceTime: 0,
        Status: this.isNew ? environment.serialService.statuses_created : (this.form.get('isActive').value ?
           environment.serialService.statuses_active : environment.serialService.statuses_blocked )
      }
    };

    this.plugins.forEach(plugin => {
      serialServiceKey.license[plugin.sysname + 'Enabled'] = this.form.controls['plugin_' + plugin.sysname].value;
      if (this.form.controls['plugin_' + plugin.sysname].value) {
        serialServiceKey.license[plugin.sysname + 'LimitTime'] = Math.floor( new Date(this.form.get('dateFinish').value).getTime() / 1000);
      } else {
        serialServiceKey.license[plugin.sysname + 'LimitTime'] = 0;
      }
    });

    this.serialSrv.addOrSetSerialKey(serialServiceKey).pipe(take(1)).subscribe(response => {
      if (response) {
        this.save_db(response.SerialNumber);
      } else {
        this.error$.next('Серийный номер не найден и не обработан.');
      }
    }, error => {
      if (error.name === 'HttpErrorResponse') {
        this.error$.next('Сервис, выдающий лицензии не доступен. ' + error.message);
      } else {
        this.error$.next(error.error);
      }
      this.submited = false;
    });
  }

}
