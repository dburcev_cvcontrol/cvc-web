import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../shared/interfaces';
import {AuthService} from '../shared/services/auth.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  form: FormGroup;
  submitted = false;
  message = '';

  constructor(
    public auth: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.queryParams.pipe(take(1)).subscribe((params: Params) => {
      if (params['loginAgain']) {
        this.message = 'Пожалуйста введите данные';
      }
    })

    this.form = new FormGroup({
      email: new FormControl(null, [
        Validators.required
      ]),
      password: new FormControl(null)
    });
  }

  submit() {
    if (this.form.invalid) {
      return ;
    }

    this.submitted = true;

    const user: User = {
      login: this.form.value.email,
      password: this.form.value.password
    };

    this.auth.login(user).pipe(take(1)).subscribe(() => {
      this.form.reset();
      this.router.navigate(['/dashboard']);
      this.submitted = false;
    }, () => {
      this.submitted = false;
    });
  }
}
