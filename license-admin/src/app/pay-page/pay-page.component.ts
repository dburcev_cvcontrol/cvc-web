import { Component, OnInit } from '@angular/core';
import {Client} from '../shared/interfaces';

@Component({
  selector: 'app-pay-page',
  templateUrl: './pay-page.component.html',
  styleUrls: ['./pay-page.component.scss']
})
export class PayPageComponent implements OnInit {

  client: Client;

  constructor() { }

  ngOnInit(): void {
  }

  setClientView(client: Client) {
    if (client) {
      this.client = client;
    }
  }
}
