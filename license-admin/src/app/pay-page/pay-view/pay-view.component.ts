import { Component, OnInit } from '@angular/core';
import {ClientService} from '../../shared/services/client.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-pay-view',
  templateUrl: './pay-view.component.html',
  styleUrls: ['./pay-view.component.scss']
})
export class PayViewComponent implements OnInit {

  constructor(
    public clnt: ClientService
  ) { }

  ngOnInit(): void {
    localStorage.setItem('last-menu', '/pays');
  }

  onRemoveClick(idPay: any) {
    this.clnt.removePay(idPay).pipe(take(1)).subscribe(() => {
      this.clnt.pays = this.clnt.pays.filter(pay => pay.id_pay !== idPay);
    });
  }

  onRemoveClientClick(idClient: any) {
    this.clnt.removeClient(this.clnt.lastSelectedClientId).pipe(take(1)).subscribe(() => {
      this.clnt.clients = this.clnt.clients.filter((client) => client.id_client !== this.clnt.lastSelectedClientId);
      this.clnt.lastSelected = null;
    });
  }
}
