import { Component, OnInit } from '@angular/core';
import {ClientService} from '../../shared/services/client.service';
import {ClientLicense, Pay, PayRow} from '../../shared/interfaces';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {take} from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-pay-edit-page',
  templateUrl: './pay-edit-page.component.html',
  styleUrls: ['./pay-edit-page.component.scss']
})
export class PayEditPageComponent implements OnInit {

  isNew = true;
  pay: Pay = {
    id_pay: uuidv4(),
    id_client: this.clnt.lastSelectedClientId,
    plan_number: '',
    plan_date: null,
    fact_number: '',
    fact_date: null,
    sum: 0,
    comment: '',
    payRows: []
  };
  licenses: ClientLicense[] = [];
  submited = false;
  form: FormGroup;

  constructor(private route: ActivatedRoute,
              public clnt: ClientService,
              private router: Router) { }

  ngOnInit(): void {
    this.clnt.getLicenses(this.clnt.getLastSelectedId()).pipe(take(1)).subscribe(licenses => {
      this.licenses = licenses;
    });
    this.route.params.pipe(take(1)).subscribe((params: Params) => {
      if (params.id === '0') {
        this.form = new FormGroup({
          plan_number: new FormControl('', Validators.required),
          plan_date: new FormControl(new Date().getTime(), Validators.required),
          fact_number: new FormControl(''),
          fact_date: new FormControl(''),
          sum: new FormControl(0),
          comment: new FormControl(''),
        });
      } else {
        this.clnt.getPayDetail(params.id).pipe(take(1)).subscribe(pay => {
          console.log(pay);
          this.pay = pay;

          if (this.pay) {
            this.isNew = false;
            this.form = new FormGroup({
              plan_number: new FormControl(pay.plan_number, Validators.required),
              plan_date: new FormControl(new Date(pay.plan_date * 1000), Validators.required),
              fact_number: new FormControl(pay.fact_number),
              fact_date: new FormControl(new Date(pay.fact_date * 1000)),
              sum: new FormControl(pay.sum),
              comment: new FormControl(pay.comment)
            });

            if (this.pay.plan_date) {
              const currentDate = new Date(this.pay.plan_date * 1000).toISOString().substring(0, 10);
              this.form.controls.plan_date.setValue(currentDate);
            }

            if (this.pay.fact_date) {
              const currentDate = new Date(this.pay.fact_date * 1000).toISOString().substring(0, 10);
              this.form.controls.fact_date.setValue(currentDate);
            }

          } else {
            this.form = new FormGroup({
              plan_number: new FormControl('', Validators.required),
              plan_date: new FormControl('', Validators.required),
              fact_number: new FormControl(''),
              fact_date: new FormControl(''),
              sum: new FormControl(0),
              comment: new FormControl(''),
            });
          }
        });
      }
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }
    this.submited = true;

    this.pay.plan_number = this.form.get('plan_number').value;
    this.pay.plan_date   = Math.floor(new Date(this.form.get('plan_date').value).getTime() / 1000);

    this.pay.fact_number = this.form.get('fact_number').value;
    this.pay.fact_date   = Math.floor(new Date(this.form.get('plan_date').value).getTime() / 1000);

    this.pay.sum = this.paySum();
    this.pay.comment = this.form.get('comment').value;

    if (!this.isNew) {
      this.clnt.updatePay(this.pay).subscribe( () => {
        const findIndex = this.clnt.pays.findIndex(pay => pay.id_pay === this.pay.id_pay);
        this.clnt.pays[findIndex] = this.pay;
        this.submited = false;
        this.router.navigate(['/pays']);
      });
    } else {
      this.clnt.addPay(this.pay).subscribe( () => {
        this.clnt.pays.push(this.pay);
        this.submited = false;
        this.router.navigate(['/pays']);
      });
    }
  }

  addRow() {
    const newRow: PayRow = {
      id_pay_row: uuidv4(),
      id_pay: this.pay.id_pay,
      name: 'Приобретение лицензии',
      serial_key: '',
      sum: 0,
      deleted: false,
      isNew: true
    };

    this.pay.payRows.push(newRow);
  }

  removeRow(idPayRow: any) {
    this.pay.payRows.forEach(row => {
      if (row.id_pay_row === idPayRow) {
        row.deleted = true;
      }
    });
  }

  payRowsVisivle(): PayRow[] {
    return this.pay.payRows.filter(row => !row.deleted);
  }

  paySum(): number {
    let sum = 0;
    this.payRowsVisivle().forEach(row => {
      sum = sum + row.sum;
    });
    return sum;
  }

  onRowNameChange(idPayRow: string, event: any) {
    this.pay.payRows.forEach(row => {
      if (row.id_pay_row === idPayRow) {
        row.name = event.target.value;
      }
    });
  }

  onRowSerialChange(idPayRow: string, event: any) {
    this.pay.payRows.forEach(row => {
      if (row.id_pay_row === idPayRow) {
        row.serial_key = event.target.value;
      }
    });
  }

  onRowSumChange(idPayRow: string, event: any) {
    this.pay.payRows.forEach(row => {
      if (row.id_pay_row === idPayRow) {
        row.sum = +event.target.value;
      }
    });
  }
}
