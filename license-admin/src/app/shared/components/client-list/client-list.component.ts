import {ClientService} from '../../services/client.service';
import {
  AfterContentInit,
  Component,
  OnInit,
} from '@angular/core';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss']
})

export class ClientListComponent implements OnInit, AfterContentInit {


  constructor(public clnt: ClientService) {
  }

  ngOnInit(): void {
    this.clnt.getGroups().pipe(take(1)).subscribe(groups => {
      this.clnt.groups = groups;
    });
    this.clnt.getClients('all').pipe(take(1)).subscribe(clients => {
      this.clnt.clients = clients;
    });
  }

  onClientClick(idClient: any) {
    if (this.clnt.clients) {
      this.clnt.clients.forEach(curClient => {
        curClient.selected = (curClient.id_client === idClient);
        if (curClient.selected) {
          this.clnt.lastSelected = curClient;
          localStorage.setItem('last-client-id', idClient.toString());
        }
      });
    }
  }

  ngAfterContentInit(): void {
    if (this.clnt.getLastSelectedId()) {
      this.onClientClick(this.clnt.getLastSelectedId());
    }
  }

}
