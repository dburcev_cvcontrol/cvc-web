import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {User} from '../interfaces';
import {Observable, Subject, throwError} from 'rxjs';
import {environment} from '../../../environments/environment';
import {catchError, tap} from 'rxjs/operators';

@Injectable()
export class AuthService {

  public error$: Subject<string> = new Subject<string>();
  public constRights = environment.rights;

  constructor(private http: HttpClient) {
  }

  // Получение токена
  get authUser(): User {
    const savedLoginStr = localStorage.getItem('auth-login');
    if (!savedLoginStr) {
      return null;
    }
    const savedLogin: User = JSON.parse(localStorage.getItem('auth-login'));
    if (new Date() > savedLogin.expareDate) {
      this.logout();
      return null;
    }
    return savedLogin;
  }

  // Вход в систему
  login(user: User): Observable<User> {
    return this.http.post<User>(environment.dataService.server +
      environment.dataService.authLoginURL, user)
      .pipe(
        tap(this.setAuthUser),
        catchError(this.handleError.bind(this))
      );
  }
  // Выход из системы
  logout() {
    this.setAuthUser(null);
  }

  private handleError(error: HttpErrorResponse) {
    this.error$.next( error.error.error );
    return throwError(error);
  }
  // Проверка на авторизацию текущего пользователя
  isAuthenticated(): boolean {
    return !!this.authUser;
  }

  hasRight(rights): boolean {
    const userRights = this.authUser.rights;
    let res = false;
    if (rights && userRights) {
      rights.push(environment.rights.superuser);
      userRights.forEach((value) => {
        if (rights.indexOf(value) !== -1) {
          res =  true;
        }
      });
    }
    return res;
  }
  // Процедура для изменения токена
  private setAuthUser(user: User) {
    if ((user) && (user.active)) {
      if (user.superuser) {
        user.rights.push('superuser');
      }
      user.expareDate = new Date(new Date().getTime() + environment.dataService.authExpire);
      user.password = '';
      localStorage.setItem('auth-login', JSON.stringify(user));
    } else {
      localStorage.setItem('auth-login', '');
    }
  }
}
