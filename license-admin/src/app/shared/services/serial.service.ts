import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {SerialDownloadInfo, SerialDownloadResult} from '../interfaces';
import {first} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class SerialService {

  constructor(private http: HttpClient) {
  }

  makeAuthHeader(): any {
    let headersObject: HttpHeaders = new HttpHeaders();
    headersObject = headersObject.append('Content-Type', 'application/json');
    headersObject = headersObject.append('Accept', '*/*');
    headersObject = headersObject.append('Authorization', 'Basic '
      + btoa( environment.serialService.login + ':' + environment.serialService.password));

    return {
      headers: headersObject,
      withCredentials: true
    };
  }

  /*********************************************************************/

  addSerialKey(serial: any): Observable<any> {
    return this.http.post(environment.serialService.server +
      environment.serialService.serialURL, serial, this.makeAuthHeader()).pipe(first());
  }

  updateSerialKey(serial: any): Observable<any> {
    return this.http.put(environment.serialService.server +
      environment.serialService.serialURL, serial, this.makeAuthHeader()).pipe(first());
  }

  addOrSetSerialKey(serial: any): Observable<any> {
    if (serial && serial.SerialNumber) {
      return this.updateSerialKey(serial);
    } else {
      return this.addSerialKey(serial);
    }
  }

  removeSerialKey(serialKey: string): Observable<any> {
    return this.http.delete(environment.serialService.server +
      environment.serialService.serialURL + `?serialnumber=${serialKey}`, this.makeAuthHeader()).pipe(first());
  }

  getSerialKey(serialDownloadInfo: SerialDownloadInfo): Observable<any> {
    return this.http.post<SerialDownloadResult>(environment.serialService.server +
      environment.serialService.getserialURL, serialDownloadInfo, this.makeAuthHeader()).pipe(first());
  }

  getSerialsKeys(): Observable<any> {
    return this.http.get(environment.serialService.server + 'api/getserialnumbers',
      this.makeAuthHeader()).pipe(first());
  }
}
