import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Client, ClientGroup, ClientLicense, ClientPlugin, Pay} from '../interfaces';
import {HttpClient} from '@angular/common/http';
import {first, take} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class ClientService {

  lastSelectedClientId: any;
  lastSelectedClient: Client;
  groups: ClientGroup[] = [];
  clients: Client[] = [];
  licenses: ClientLicense[] = [];
  plugins: ClientPlugin[] = [];
  pays: Pay[] = [];
  cont = false;

  constructor(private http: HttpClient) {
    this.getGroups().subscribe(groups => {
      this.groups = groups;
    });
  }

  /*********************************************************************/

  getLastSelectedId(): string {
      if (this.lastSelectedClientId) {
        return this.lastSelectedClientId;
      } else {
        return localStorage.getItem('last-client-id');
      }
  }

  set lastSelected(client: Client) {
    if (client) {
      this.lastSelectedClientId = client.id_client;
      localStorage.setItem('last-client-id', client.id_client);
    } else {
      this.lastSelectedClientId = null;
      localStorage.setItem('last-client-id', '');
    }

    this.lastSelectedClient = client;

    if (this.lastSelectedClient) {
      this.getLicenses(this.lastSelectedClient.id_client).pipe(take(1)).subscribe((licenses) => {
        this.licenses = licenses;
      });

      this.getPays(this.lastSelectedClient.id_client).pipe(take(1)).subscribe(pays => {
        this.pays = pays;
      });
    }
  }

  /*********************************************************************/

  getGroups(): Observable<ClientGroup[]> {
    return this.http.get<ClientGroup[]>(environment.dataService.server +
      environment.dataService.groupsURL).pipe(first());
  }

  getGroupDetail(idGroup: any): Observable<ClientGroup> {
    return this.http.get<ClientGroup>(environment.dataService.server +
      environment.dataService.groupsURL + `detail/${idGroup}`).pipe(first());
  }

  addGroup(group: ClientGroup): Observable<any> {
    return this.http.post(environment.dataService.server +
      environment.dataService.groupsURL, group).pipe(first());
  }

  updateGroup(group: ClientGroup): Observable<any> {
    return this.http.put(environment.dataService.server +
      environment.dataService.groupsURL, group).pipe(first());
  }

  removeGroup(idGroup: any): Observable<any> {
    return this.http.delete(environment.dataService.server +
      environment.dataService.groupsURL + `${idGroup}`).pipe(first());
  }

  /*********************************************************************/

  getClients(idGroup: any): Observable<Client[]> {
    return this.http.get<Client[]>(environment.dataService.server +
      environment.dataService.clientsURL + `${idGroup}`).pipe(first());
  }

  getClientDetail(idClient: any): Observable<Client> {
    return this.http.get<Client>(environment.dataService.server +
      environment.dataService.clientsURL + `detail/${idClient}`).pipe(first());
  }

  addClient(client: Client): Observable<any> {
    return this.http.post(environment.dataService.server +
      environment.dataService.clientsURL, client).pipe(first());
  }

  updateClient(client: Client): Observable<any> {
    return this.http.put(environment.dataService.server +
      environment.dataService.clientsURL, client).pipe(first());
  }

  removeClient(idClient: any): Observable<any> {
    return this.http.delete(environment.dataService.server +
      environment.dataService.clientsURL + `${idClient}`).pipe(first());
  }

  /*********************************************************************/

  getLicenses(idClient: any): Observable<ClientLicense[]> {
    return this.http.get<ClientLicense[]>(environment.dataService.server +
      environment.dataService.licensesURL + `${idClient}`).pipe(first());
  }

  getLicenseDetail(idClientLicense: any): Observable<ClientLicense> {
    return this.http.get<ClientLicense>(environment.dataService.server +
      environment.dataService.licensesURL + `detail/${idClientLicense}`).pipe(first());
  }

  addLicense(clientLicense: ClientLicense): Observable<any> {
    return this.http.post(environment.dataService.server +
      environment.dataService.licensesURL, clientLicense).pipe(first());
  }

  updateLicense(clientLicense: ClientLicense): Observable<any> {
    return this.http.put(environment.dataService.server +
      environment.dataService.licensesURL, clientLicense).pipe(first());
  }

  removeLicense(idClientLicense: any): Observable<any> {
    return this.http.delete(environment.dataService.server +
      environment.dataService.licensesURL + `${idClientLicense}`).pipe(first());
  }

  /*********************************************************************/

  getPlugins(): Observable<ClientPlugin[]> {
    return this.http.get<ClientPlugin[]>(environment.dataService.server +
      environment.dataService.pluginsURL).pipe(first());
  }

  getPluginDetail(idPlugin: any): Observable<ClientPlugin> {
    return this.http.get<ClientPlugin>(environment.dataService.server +
      environment.dataService.pluginsURL + `detail/${idPlugin}`).pipe(first());
  }

  addPlugin(plugin: ClientPlugin): Observable<any> {
    return this.http.post(environment.dataService.server +
      environment.dataService.pluginsURL, plugin).pipe(first());
  }

  updatePlugin(plugin: ClientPlugin): Observable<any> {
    return this.http.put(environment.dataService.server +
      environment.dataService.pluginsURL, plugin).pipe(first());
  }

  removePlugin(idPlugin: any): Observable<any> {
    return this.http.delete(environment.dataService.server +
      environment.dataService.pluginsURL + `${idPlugin}`).pipe(first());
  }

  /*********************************************************************/

  getPays(idClient: any): Observable<Pay[]> {
    return this.http.get<Pay[]>(environment.dataService.server +
      environment.dataService.paysURL + `${idClient}`).pipe(first());
  }

  getPayDetail(idPay: any): Observable<Pay> {
    return this.http.get<Pay>(environment.dataService.server +
      environment.dataService.paysURL + `detail/${idPay}`).pipe(first());
  }

  addPay(pay: Pay): Observable<any> {
    return this.http.post(environment.dataService.server +
      environment.dataService.paysURL, pay).pipe(first());
  }

  updatePay(pay: Pay): Observable<any> {
    return this.http.put(environment.dataService.server +
      environment.dataService.paysURL, pay).pipe(first());
  }

  removePay(idPay: any): Observable<any> {
    return this.http.delete(environment.dataService.server +
      environment.dataService.paysURL + `${idPay}`).pipe(first());
  }
}
