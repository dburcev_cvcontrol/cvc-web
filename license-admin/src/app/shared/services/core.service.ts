import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Right, Role} from '../core.interfaces';
import {first, tap} from 'rxjs/operators';
import {User} from '../interfaces';

@Injectable({providedIn: 'root'})
export class CoreService {

  rights: Right[] = [];
  roles: Role[] = [];
  users: User[] = [];

  constructor(private http: HttpClient) {}

  getRights(force: boolean = false): Observable<Right[]> {
    if (force || this.rights.length === 0) {
      return this.http.get<Right[]>(environment.dataService.server +
        environment.dataService.rightsURL).pipe(first()).pipe(
            tap<Right[]>(rights => this.rights = rights)
        );
    } else {
      return of<Right[]>(this.rights);
    }
  }

  getRightDetail(idRight: number): Observable<Right> {
    return this.http.get<Right>(environment.dataService.server +
      environment.dataService.rightsURL + `detail/${idRight}`).pipe(first());
  }

  addRight(right: Right): Observable<any> {
    return this.http.post(environment.dataService.server +
      environment.dataService.rightsURL, right).pipe(first()).pipe(
        tap(item => {
          this.rights.push(item);
        })
    );
  }

  updateRight(right: Right): Observable<any> {

    return this.http.put(environment.dataService.server +
      environment.dataService.rightsURL, right).pipe(first()).pipe(
        tap(item => {
            const itemIndex = this.rights.findIndex(findItem => findItem.id_right === right.id_right);

            if (itemIndex !== -1) {
              this.rights[itemIndex] = item;
            }
          }
        )
    );
  }

  addOrUpdateRight(right: Right): Observable<any> {
    if (right.id_right) {
      return this.updateRight(right);
    } else {
      return this.addRight(right);
    }
  }

  removeRight(idRight: number): Observable<any> {
    return this.http.delete(environment.dataService.server +
      environment.dataService.rightsURL + `${idRight}`).pipe(first()).pipe(
        tap(() => {
            this.rights = this.rights.filter(findItem => findItem.id_right !== idRight);
        })
    );
  }

  /***********************************************************************/

  getRoles(force: boolean = false): Observable<Role[]> {
    if (force || this.roles.length === 0) {
      return this.http.get<Role[]>(environment.dataService.server +
        environment.dataService.rolesURL).pipe(first()).pipe(
          tap<Role[]>(roles => {
            this.roles = roles;
          })
      );
    } else {
      return of<Role[]>(this.roles);
    }
  }

  getRoleDetail(idRole: number): Observable<Role> {
    return this.http.get<Role>(environment.dataService.server +
      environment.dataService.rolesURL + `detail/${idRole}`).pipe(first());
  }

  addRole(role: Role): Observable<any> {
    return this.http.post(environment.dataService.server +
      environment.dataService.rolesURL, role).pipe(first()).pipe(
        tap(item => {
          role.id_role = item.id_role;
          this.roles.push(role);
        })
    );
  }

  updateRole(role: Role): Observable<any> {
    return this.http.put(environment.dataService.server +
      environment.dataService.rolesURL, role).pipe(first()).pipe(
        tap(() => {
          this.roles = this.roles.filter(findItem => findItem.id_role !== role.id_role);
          this.roles.push(role);
        })
    );
  }

  addOrUpdateRole(role: Role): Observable<any> {
    if (role.id_role) {
      return this.updateRole(role);
    } else {
      return this.addRole(role);
    }
  }

  removeRole(idRole: number): Observable<any> {
    return this.http.delete(environment.dataService.server +
      environment.dataService.rolesURL + `${idRole}`).pipe(first()).pipe(
        tap(() => {
          this.roles = this.roles.filter(findItem => findItem.id_role !== idRole);
        })
    );
  }

  /***********************************************************************/

  getUsers(force: boolean = false): Observable<User[]> {
    if (force || this.users.length === 0) {
      return this.http.get<User[]>(environment.dataService.server +
        environment.dataService.usersURL).pipe(first()).pipe(
        tap<User[]>(users => {
          this.users = users;
        })
      );
    } else {
      return of<User[]>(this.users);
    }
  }

  getUserDetail(idUser: number): Observable<User> {
    return this.http.get<User>(environment.dataService.server +
      environment.dataService.usersURL + `detail/${idUser}`).pipe(first());
  }

  addUser(user: User): Observable<any> {
    return this.http.post(environment.dataService.server +
      environment.dataService.usersURL, user).pipe(first()).pipe(
      tap(item => {
        user.id_user = item.id_user;
        this.users.push(user);
      })
    );
  }

  updateUser(user: User): Observable<any> {
    return this.http.put(environment.dataService.server +
      environment.dataService.usersURL, user).pipe(first()).pipe(
      tap(() => {
        this.users = this.users.filter(findItem => findItem.id_user !== user.id_user);
        this.users.push(user);
      })
    );
  }

  addOrUpdateUser(user: User): Observable<any> {
    if (user.id_user) {
      return this.updateUser(user);
    } else {
      return this.addUser(user);
    }
  }

  removeUser(idUser: number): Observable<any> {
    return this.http.delete(environment.dataService.server +
      environment.dataService.usersURL + `${idUser}`).pipe(first()).pipe(
      tap(() => {
        this.users = this.users.filter(findItem => findItem.id_user !== idUser);
      })
    );
  }

  resetPassword(userReset) {
    return this.http.post(environment.dataService.server +
      environment.dataService.usersURL + `resetpassword`, userReset).pipe(first());
  }
}
