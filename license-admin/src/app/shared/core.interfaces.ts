export interface Right {
  id_right?: number;
  sysname: string;
  name: string;
}

export interface Role {
  id_role?: number;
  name: string;
  rights?: Right[];
}

