export interface User {
  id_user?: number;
  login: string;
  password?: string;
  name?: string;
  comment?: string;
  active?: boolean;
  superuser?: boolean;
  rights?: any;
  roles?: any;
  expareDate?: any;
}

export interface ClientGroup {
  id_group: any;
  name: string;
  clients?: Client[];
  expanded?: boolean;
}

export interface Client {
  id_client?: any;
  id_group?: any;
  name: string;
  json_data?: any;
  selected?: boolean;
}

export interface ClientLicense {
  id_license?: any;
  id_client: any;
  serial_key: string;
  date_finish?: any;
  active?: boolean;
  plugins?: any;
  pluginsJson?: ClientPlugin[];
}

export interface ClientPlugin {
  id_plugin?: any;
  sysname: string;
  name: string;
  comment?: string;
}

export interface PayRow {
  id_pay_row?: string;
  id_pay: string;
  name: string;
  serial_key?: string;
  sum?: number;
  sort_order?: number;
  deleted?: boolean;
  isNew?: boolean;
}

export interface Pay {
  id_pay?: string;
  id_client: string;
  plan_number?: string;
  plan_date?: number;
  fact_number?: string;
  fact_date?: number;
  comment?: string;
  sum?: number;
  sort_order?: number;
  payRows?: PayRow[];
}

export interface SerialDownloadInfo {
  HID: string; // Фалй c2v в формате base64
  SerialNumber: string;
  ClientID: string;
}

export interface SerialDownloadResult {
  License: string; // Файл v2c в формате base64
  SerialNumber: string;
  Error: string;
  Status: string;
}
