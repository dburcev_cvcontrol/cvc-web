import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Client, ClientGroup} from '../shared/interfaces';
import {ClientService} from '../shared/services/client.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {take} from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-client-page',
  templateUrl: './client-page.component.html',
  styleUrls: ['./client-page.component.scss']
})
export class ClientPageComponent implements OnInit {

  lastNavigate = localStorage.getItem('last-menu') ? localStorage.getItem('last-menu') : '/dashboard';

  client: Client;
  groups: ClientGroup[];
  form: FormGroup;
  submited = false;

  constructor(private route: ActivatedRoute,
              private clnt: ClientService,
              private router: Router) { }

  ngOnInit(): void {
    this.clnt.getGroups().pipe(take(1)).subscribe(groups => {
      this.groups = groups;
    });

    this.route.params.pipe(take(1)).subscribe((params: Params) => {
      if (params.id === '0') {
        this.client = {
          name: 'Новый клиент'
        };

        this.form = new FormGroup({
          name: new FormControl('Новый клиент', Validators.required),
          id_group: new FormControl(null, Validators.required),
        });
      } else {
        this.clnt.getClientDetail(params.id).pipe(take(1)).subscribe((client) => {
          this.client = client;

          if (this.client) {
            this.form = new FormGroup({
              name: new FormControl(this.client.name, Validators.required),
              id_group: new FormControl(this.client.id_group, Validators.required),
            });
          } else {
            this.form = new FormGroup({
              name: new FormControl('Новый клиент', Validators.required),
              id_group: new FormControl(null, Validators.required),
            });
          }
        });
      }
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }
    this.submited = true;

    const newClient: Client = {
      id_client: this.client.id_client ? this.client.id_client : uuidv4(),
      name: this.form.get('name').value,
      id_group: this.form.get('id_group').value
    };

    if (this.client && this.client.id_group) {
      this.clnt.updateClient(newClient).subscribe( () => {
        this.submited = false;
        this.router.navigate([this.lastNavigate]);
      });
    } else {
      this.clnt.addClient(newClient).subscribe( () => {
        this.submited = false;
        localStorage.setItem('last-client-id', newClient.id_client);
        this.router.navigate([this.lastNavigate]);
      });
    }
  }


}
