const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const env = require('./app/config/env');

const app = express();

let corsOptions = {
     origin: "http://localhost:4200"
};

console.clear();

app.use(cors(corsOptions));

//Локальный логгер для отображения получаемыз запросов от клиентов
const morgan = require('morgan');
app.use(morgan('tiny'));

// Для распарсивания тела запросов в JSON
app.use(bodyParser.json());

// Для расширенного распарсивания - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require('./app/models');
db.sequelize
    .authenticate()
    .then(() => {
        console.log('Соединение с БД "%s@%s" прошло успешно.', env.database.host, env.database.database);
    })
    .catch(err => {
        console.error('Ошибка подключения к БД "$%s@%s": %s', env.database.host, env.database.database, err);
    });

db.sequelize.query('create schema if not exists core;' +
    'create schema if not exists licenses;').then(()=> {
  db.sequelize.sync({}).then(() => {
    require('./app/models/start.data')(db);
  });
});

//CORE
const routerAuth = require('./app/routes/core.auth');
app.use('/api/auth', routerAuth);

const routerRights = require('./app/routes/core.rights');
app.use('/api/core/rights', routerRights);

const routerRoles = require('./app/routes/core.roles');
app.use('/api/core/roles', routerRoles);

const routerUsers = require('./app/routes/core.users');
app.use('/api/core/users', routerUsers);

//LICENSES
const routerLicenseGroups = require('./app/routes/licenses.groups');
app.use('/api/groups', routerLicenseGroups);

const routerLicensePlugins = require('./app/routes/licenses.plugins');
app.use('/api/plugins', routerLicensePlugins);

const routerLicenseClients = require('./app/routes/licenses.clients');
app.use('/api/clients', routerLicenseClients);

const routerLicenseLicenses = require('./app/routes/licenses.licenses');
app.use('/api/licenses', routerLicenseLicenses);

const routerLicensePay = require('./app/routes/licenses.pay');
app.use('/api/pay', routerLicensePay);

const routerHelp = require('./app/routes/help');
app.use('/', routerHelp);

//Запуск сервера
app.listen(env.webserver.port, env.webserver.address, function () {
    console.log("Сервер запущен http://%s:%s", env.webserver.address, env.webserver.port);
});
