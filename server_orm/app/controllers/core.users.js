//Подключаем базу данных
const db = require('../models');
const md5 = require('md5');
const User = db.users;
const Role = db.roles;

// *******************************************************************

module.exports = {
    getUsers: async (request, response) => {
        await User.findAll({
            order: [
              ['login']
            ],
            include: [
                {model: Role}
            ]
        }).then(users => {
            response.status(200).json(users);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    getUser: async (request, response) => {
        await User.findByPk(request.params.id, {
            include: [
                {model: Role}
            ]
        }).then( user => {
            response.status(200).json(user);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    addUser: async (request, response) => {
        await User.create({
            login: request.body.login,
            password: request.body.password,
            name: request.body.name,
            comment: request.body.comment,
            active: request.body.active,
            superuser: request.body.superuser
        }).then(user => {
            const promises = [];
            if (request.body.roles) {
                request.body.roles.forEach(role => {
                    promises.push(
                        Role.findByPk(role.id_role).then(item => {
                            user.addRole(item);
                        })
                    );
                });
            }
            Promise.all(promises).then(() => {
                user.save().then(result => {
                    response.status(200).json(result);
                });
            });
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    updateUser: async (request, response) => {
        let user = await User.findByPk(request.body.id_user,
            {
                include: [
                    {model: Role}
                ]
            });
        if (user) {
            await user.update(request.body).then(() => {
                const promises = [];
                if (request.body.roles) {
                    promises.push(user.setRoles([]));
                    request.body.roles.forEach(role => {
                        promises.push(
                            Role.findByPk(role.id_role).then(item => {
                                user.addRole(item);
                            })
                        );
                    });
                }
                Promise.all(promises).then(() => {
                    user.save().then(result => {
                        response.status(200).json(result);
                    });
                });
            }).catch( reason => {
                response.status(400).json({error: `${reason}`})
            });
        } else {
            response.status(401).json({error: `Пользователь с ключем ${request.body.id_user} не найден`})
        }
    },

    removeUser: async (request, response) => {
        let user = await User.findByPk(request.params.id);
        if (user) {
            user.destroy();
            response.status(200).json({message: 'Пользователь удален'})
        } else {
            response.status(401).json({error: `Пользователь с ключем ${request.params.id} не найден`})
        }
    },

    resetPassword: async (request, response) => {
        let user = await User.findByPk(request.body.id_user);
        if (user) {
            user.password = md5( request.body.password );
            user.save();
            response.status(200).json({message: 'Пароль успешно изменен'})
        } else {
            response.status(401).json({error: `Пользователь с ключем ${request.body.id_user} не найден`})
        }
    }
};

