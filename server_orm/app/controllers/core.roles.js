//Подключаем базу данных
const db = require('../models');
const Role = db.roles;
const Right = db.rights;

// *******************************************************************

module.exports = {
    getRoles: async (request, response) => {
        await Role.findAll({
            order:[
              ['name']
            ],
            include: [
                {model: Right}
            ]
        }).then(roles => {
            response.status(200).json(roles);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    getRole: async (request, response) => {
        await Role.findByPk(request.params.id, {
            include: [
                {model: Right}
            ]
        }).then( role => {
            response.status(200).json(role);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    addRole: async (request, response) => {
        await Role.create({
            id_role: request.body.id_role,
            name: request.body.name,
        }).then(role => {
            const promises = [];
            if (request.body.rights) {
                request.body.rights.forEach(right => {
                    promises.push(
                        Right.findByPk(right.id_right).then(item => {
                            role.addRight(item);
                        })
                    );
                });
            }
            Promise.all(promises).then(() => {
                role.save().then(result => {
                    response.status(200).json(result);
                });
            });
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    updateRole: async (request, response) => {
        let role = await Role.findByPk(request.body.id_role,
            {
                include: [
                    {model: Right}
                ]
            });
        if (role) {
            await role.update(request.body).then(() => {
                const promises = [];
                if (request.body.rights) {
                    promises.push(role.setRights([]));
                    request.body.rights.forEach(right => {
                        promises.push(
                            Right.findByPk(right.id_right).then(item => {
                                role.addRight(item);
                            })
                        );
                    });
                }
                Promise.all(promises).then(() => {
                    role.save().then(result => {
                        response.status(200).json(result);
                    });
                });
            }).catch( reason => {
                response.status(400).json({error: `${reason}`})
            });
        } else {
            response.status(401).json({error: `Роль с ключем ${request.body.id_role} не найдена`})
        }
    },

    removeRole: async (request, response) => {
        let role = await Role.findByPk(request.params.id);
        if (role) {
            role.destroy();
            response.status(200).json({message: 'Роль удалена'})
        } else {
            response.status(401).json({error: `Роль с ключем ${request.params.id} не найдена`})
        }
    }
};

