//Подключаем базу данных
const db = require('../models');
const License = db.licenses;
const Plugin = db.plugins;

module.exports = {
    getLicenses: async (request, response) => {
        console.log(request.params);
        if (request.params.id === 'all') {
            await License.findAll().then(licenses => {
                response.status(200).json(licenses);
            }).catch(reason => {
                response.status(400).json({error: `${reason}`})
            });
        } else {
            await License.findAll({
                where: {
                    id_client: request.params.id
                },
                include: [
                    {model: Plugin}
                ]
            }).then(licenses => {
                response.status(200).json(licenses);
            }).catch(reason => {
                response.status(400).json({error: `${reason}`})
            });
        }
    },

    getLicense: async (request, response) => {
        await License.findOne({
            where: {
                id_license: request.params.id
            },
            include: [
                {model: Plugin}
            ]
        }).then(licenses => {
            response.status(200).json(licenses);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    addLicense: async (request, response) => {

        console.log(request.body);

        let promises = [];
        let license = await License.create({
            id_license: request.body.id_license,
            id_client: request.body.id_client,
            serial_key: request.body.serial_key,
            date_finish: request.body.date_finish,
            active: request.body.active
        });

        const plugins = request.body.plugins;

        plugins.forEach(plugin => {
            promises.push(
                Plugin.findByPk(plugin).then(item => {
                    license.addPlugin(item);
                    license.save();
                })
            );
        });

        Promise.all(promises).then(()=> {
            response.status(200).json(license);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    updateLicense: async (request, response) => {
        let license = await License.findByPk(request.body.id_license);
        if (license) {
            await license.update(request.body).then(() => {
                let promises = [];
                let plugins = request.body.plugins;
                promises.push(license.setPlugins([]));

                plugins.forEach(plugin => {
                   promises.push(
                       Plugin.findByPk(plugin).then(resultPlugin => {
                           license.addPlugin(resultPlugin);
                       })
                   );
                });

                Promise.all(promises).then(() => {
                    license.save();
                    response.status(200).json(license);
                });
            }).catch( reason => {
                response.status(400).json({error: `${reason}`})
            });
        } else {
            response.status(401).json({error: `Лицензия с ключем ${request.body.id_license} не найдена`})
        }
    },

    removeLicense: async (request, response) => {
        let license = await License.findByPk(request.params.id);
        if (license) {
            license.destroy();
            response.status(200).json({message: 'Лицензия удалена'})
        } else {
            response.status(401).json({error: `Лицензия с ключем ${request.params.id} не найдена`})
        }
    },

};
