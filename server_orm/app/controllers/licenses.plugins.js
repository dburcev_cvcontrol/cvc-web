//Подключаем базу данных
const db = require('../models');
const Plugin = db.plugins;

module.exports = {
    getPlugins: async (request, response) => {
        await Plugin.findAll().then(plugins => {
            response.status(200).json(plugins);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    getPlugin: async (request, response) => {
        await Plugin.findByPk(request.params.id).then( plugin => {
            response.status(200).json(plugin);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    addPlugin: async (request, response) => {
        await Plugin.create({
            id_plugin: request.body.id_group,
            sysname: request.body.sysname,
            name: request.body.name,
            comment: request.body.comment
        }).then(plugin => {
            response.status(200).json(plugin);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    updatePlugin: async (request, response) => {
        let plugin = await Plugin.findByPk(request.body.id_plugin);
        if (plugin) {
            await plugin.update(request.body).then(() => {
                response.status(200).json(plugin);
            }).catch( reason => {
                response.status(400).json({error: `${reason}`})
            });
        } else {
            response.status(401).json({error: `Модуль с ключем ${request.body.id_plugin} не найден`})
        }
    },

    removePlugin: async (request, response) => {
        let plugin = await Plugin.findByPk(request.params.id);
        if (plugin) {
            plugin.destroy();
            response.status(200).json({message: 'Модуль удален'})
        } else {
            response.status(401).json({error: `Модуль с ключем ${request.params.id} не найден`})
        }
    }
};
