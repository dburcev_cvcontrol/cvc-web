//Подключаем базу данных
const db = require('../models');
const Pay = db.pays;
const PayRow = db.payRows;

module.exports = {
    getPays: async (request, response) => {
        await Pay.findAll({
            where: {
                id_client: request.params.id
            },
            include: [
                {model: PayRow}
            ]
        }).then(pays => {
            response.status(200).json(pays);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    getPay: async (request, response) => {
        await Pay.findOne({
            where: {
                id_pay: request.params.id
            },
            include: [
                {model: PayRow}
            ]
        }).then(pays => {
            response.status(200).json(pays);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    addPay: async (request, response) => {
        let promises = [];
        let resPay;

        promises.push(
            Pay.create({
                id_pay: request.body.id_pay,
                id_client: request.body.id_client,
                plan_number: request.body.plan_number,
                plan_date: request.body.plan_date,
                fact_number: request.body.fact_number,
                fact_date: request.body.fact_date,
                comment: request.body.comment,
                sum: request.body.sum
            }).then(pay => {
                resPay = pay;
            })
        );

        if (request.body.rows) {
            request.body.rows.forEach(row => {
                if (!row.deleted) {
                    promises.push(
                        PayRow.create({
                           id_pay_row: row.id_pay_row,
                           name: row.name,
                           serial_key: row.serial_key,
                           sum: row.sum
                        }).then(payRow => {
                            resPay.addPayRow(payRow);
                        })
                    );
                }
            })
        }

        Promise.all(promises).then(() => {
            resPay.save();
            response.status(200).json(resPay);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    updatePay: async (request, response) => {
        let pay = await Pay.findByPk(request.body.id_pay);
        if (pay) {
            await pay.update(request.body).then(pay => {
                let promises = [];

                if (request.body.payRows) {
                    request.body.payRows.forEach(row => {
                        if (!row.deleted && row.isNew) {
                            promises.push(
                              PayRow.create({
                                 id_pay_row: row.id_pay_row,
                                 id_pay: row.id_pay,
                                 name: row.name,
                                 serial_key: row.serial_key,
                                 sum: row.sum
                              })
                            );
                        } else if (!row.deleted && !row.isNew) {
                            promises.push(
                                PayRow.findByPk(row.id_pay_row).then(payRow => {
                                    payRow.update(row);
                                })
                            );
                        } else if (!row.isNew && row.deleted) {
                            promises.push(
                                PayRow.findByPk(row.id_pay_row).then(payRow => {
                                   payRow.destroy();
                                })
                            );
                        }
                    })
                }

                Promise.all(promises).then(() => {
                    pay.save();
                    response.status(200).json(pay);
                });
            }).catch( reason => {
                response.status(400).json({error: `${reason}`})
            });
        } else {
            response.status(401).json({error: `Счет с ключем ${request.body.id_pay} не найден`})
        }
    },

    removePay: async (request, response) => {
        let pay = await Pay.findByPk(request.params.id);
        if (pay) {
            pay.destroy();
            response.status(200).json({message: 'Счет удален'})
        } else {
            response.status(401).json({error: `Счет с ключем ${request.params.id} не найден`})
        }
    }

};
