//Подключаем базу данных
const db = require('../models');
const ClientGroup = db.clientGroups;

// *******************************************************************

module.exports = {
    getGroups: async (request, response) => {
        await ClientGroup.findAll().then(groups => {
            response.status(200).json(groups);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    getGroup: async (request, response) => {
        await ClientGroup.findByPk(request.params.id).then( group => {
           response.status(200).json(group);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    addGroup: async (request, response) => {
        await ClientGroup.create({
            id_group: request.body.id_group,
            name: request.body.name
        }).then(group => {
            response.status(200).json(group);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    updateGroup: async (request, response) => {
        let group = await ClientGroup.findByPk(request.body.id_group);
        if (group) {
            await group.update(request.body).then(() => {
                response.status(200).json(group);
            }).catch( reason => {
                response.status(400).json({error: `${reason}`})
            });
        } else {
            response.status(401).json({error: `Группа с ключем ${request.body.id_group} не найдена`})
        }
    },

    removeGroup: async (request, response) => {
        let group = await ClientGroup.findByPk(request.params.id);
        if (group) {
            group.destroy();
            response.status(200).json({message: 'Группа удалена'})
        } else {
            response.status(401).json({error: `Группа с ключем ${request.params.id} не найдена`})
        }
    }
};

