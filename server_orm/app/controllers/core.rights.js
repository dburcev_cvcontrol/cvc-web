//Подключаем базу данных
const db = require('../models');
const Right = db.rights;

// *******************************************************************

module.exports = {
    getRights: async (request, response) => {
        await Right.findAll({
            order: [
                ['name']
            ]
        }).then(rights => {
            response.status(200).json(rights);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    getRight: async (request, response) => {
        await Right.findByPk(request.params.id).then( right => {
            response.status(200).json(right);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    addRight: async (request, response) => {
        await Right.create({
            id_right: request.body.id_right,
            sysname: request.body.sysname,
            name: request.body.name
        }).then(right => {
            response.status(200).json(right);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    updateRight: async (request, response) => {
        let right = await Right.findByPk(request.body.id_right);
        if (right) {
            await right.update(request.body).then(() => {
                response.status(200).json(right);
            }).catch( reason => {
                response.status(400).json({error: `${reason}`})
            });
        } else {
            response.status(401).json({error: `Право с ключем ${request.body.id_right} не найдено`})
        }
    },

    removeRight: async (request, response) => {
        let right = await Right.findByPk(request.params.id);
        if (right) {
            right.destroy();
            response.status(200).json({message: 'Право удалено'})
        } else {
            response.status(401).json({error: `Право с ключем ${request.params.id} не найдено`})
        }
    }
};

