//Подключаем базу данных
const db = require('../models');
const md5 = require('md5');
const User = db.users;
const Op = db.Sequelize.Op;

function getAllMethods(object) {
    return Object.getOwnPropertyNames(object).filter(function(property) {
        return typeof object[property] == 'function';
    });
}

//Контроллер для пользователей
module.exports.login = async function(request, response) {

    const user = await User.findOne({
        where: {
            login: request.body.login,
        }
    });

    if (!user) {
        response.status(400).json({error: 'Пользователь не найден'})
    } else if (!user.get('active')) {
        response.status(401).json({error: 'Пользователь не активен'})
    } else if ((user.password !== null) && (user.password !== md5(request.body.password))) {
        response.status(402).json({error: 'Пароль указан неверно'})
    } else {
        let resUser = user.toJSON();
        resUser.rights = await user.rights;
        response.status(200).json(resUser);
    }

};
