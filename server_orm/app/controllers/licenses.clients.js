//Подключаем базу данных
const db = require('../models');
const Client = db.clients;

module.exports = {
    getClients: async (request, response) => {
        if (request.params.id === 'all') {
            await Client.findAll().then(result => {
                response.status(200).json(result);
            }).catch( reason => {
                response.status(400).json({error: `${reason}`})
            });
        } else {
            await Plugin.findAll({where: {id_client: request.params.id}}).then(clients => {
                response.status(200).json(clients);
            }).catch( reason => {
                response.status(400).json({error: `${reason}`})
            });
        }
    },

    getClient: async (request, response) => {
        await Client.findByPk(request.params.id).then( client => {
            response.status(200).json(client);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    addClient: async (request, response) => {
        await Client.create({
            id_client: request.body.id_client,
            id_group: request.body.id_group,
            name: request.body.name,
            json_data: request.body.json_data
        }).then(client => {
            response.status(200).json(client);
        }).catch( reason => {
            response.status(400).json({error: `${reason}`})
        });
    },

    updateClient: async (request, response) => {
        let client = await Client.findByPk(request.body.id_client);
        if (client) {
            await client.update(request.body).then(() => {
                response.status(200).json(client);
            }).catch( reason => {
                response.status(400).json({error: `${reason}`})
            });
        } else {
            response.status(401).json({error: `Клиент с ключем ${request.body.id_client} не найден`})
        }
    },

    removeClient: async (request, response) => {
        let client = await Client.findByPk(request.params.id);
        if (client) {
            client.destroy();
            response.status(200).json({message: 'Клиент удален'})
        } else {
            response.status(401).json({error: `Клиент с ключем ${request.params.id} не найден`})
        }
    }
};
