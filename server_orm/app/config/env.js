//Файл конфигурации
const env = {
    //Параметры вебсервера
    webserver: {
        //Адрес прослушивания
        address: "localhost",
        //Порт прослушивания
        port: 4300,
    },
    //Параметры подключения к базе данных
    database: {
        //Адрес сервера
        host: 'localhost',
        // host: 'license.cvcontrol.tech',
        //Наименование базы данных
        database: 'licensing',
        //Порт
        port: 5432,
        //Пользователь
        user: 'postgres',
        //Пароль
        password: 'postgresql',
         // password: 'Qapb52Le',
        //Диалект
        dialect: 'postgres',
        //Параметры пула соединения
        pool: {
            max: 20,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    }
};

module.exports = env;
