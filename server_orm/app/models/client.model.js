module.exports = (sequelize, Sequelize) => {
    class Client extends Sequelize.Model {}

    Client.init({
        id_client: {
            type: Sequelize.UUID,
            allowNull: false,
            primaryKey: true,
            defaultValue: Sequelize.UUIDV4
        },
        name: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        json_data: {
            type: Sequelize.TEXT
        }
    }, {
        sequelize,
        modelName: 'client',
        schema: 'licenses',
        underscored: true
    });

    return Client;
};
