module.exports = (sequelize, Sequelize) => {
    class ClientGroup extends Sequelize.Model {}

    ClientGroup.init({
        id_group: {
            type: Sequelize.UUID,
            allowNull: false,
            primaryKey: true,
            defaultValue: Sequelize.UUIDV4
        },
        name: {
            type: Sequelize.TEXT,
            allowNull: false
        }
    }, {
        sequelize,
        modelName: 'clientGroup',
        schema: 'licenses',
        underscored: true
    });

    return ClientGroup;
};
