module.exports = async (db) => {

    const User = db.users;
    const Right = db.rights;
    const Role = db.roles;
    let admin;
    let licenseRight;
    let payRight;
    let licenseRole;
    let payRole;

    await User.findOrCreate({
        where: {
            superuser: true
        },
        defaults: {
            login: 'admin@cvcontrol.tech',
            name: 'Администратор',
            superuser: true,
            active: true
        }
    }).then((user, created) => {
        if (created) {
            console.log('Создан новый пользователь "Администратор"')
        }
        admin = user[0];
    });

    await Right.findOrCreate({
        where: {
            sysname: 'licenses'
        }, defaults: {
            sysname: 'licenses',
            name: 'Управление лицензиями'
        }
    }).then((right, create) => {
        if (create) {
            console.log('Создано обязательное право "Управление лицензиями"');
        }
        licenseRight = right[0];
    });

    await Right.findOrCreate({
        where: {
            sysname: 'pay'
        }, defaults: {
            sysname: 'pay',
            name: 'Управление оплатой'
        }
    }).then((right, create) => {
        if (create) {
            console.log('Создано обязательное право "Управление оплатой"');
        }
        payRight = right[0];
    });

    await Role.findOrCreate({
        where: {
            name: 'Управление лицензиями'
        }, defaults: {
            name: 'Управление лицензиями'
        }
    }).then((role, created) => {
        if (created) {
            console.log('Создана обязательная роль "Управление лицензиями"')
        }
        licenseRole = role[0];
        licenseRole.addRight(licenseRight);
        licenseRole.save();
    });

    await Role.findOrCreate({
        where: {
            name: 'Управление оплатой'
        }, defaults: {
            name: 'Управление оплатой'
        }
    }).then((role, created) => {
        if (created) {
            console.log('Создана обязательная роль "Управление оплатой"')
        }
        payRole = role[0];
        payRole.addRight(payRight);
        payRole.save();
    });

    if (admin.get('login') === 'admin@cvcontrol.tech') {
        console.log('Добавляем роли для администратора');
        admin.addRole(payRole);
        admin.addRole(licenseRole);
        admin.save();
    }

};
