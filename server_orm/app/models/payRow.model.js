module.exports = (sequelize, Sequelize) => {
    class PayRow extends Sequelize.Model {}

    PayRow.init({
        id_pay_row: {
            type: Sequelize.UUID,
            allowNull: false,
            primaryKey: true,
            defaultValue: Sequelize.UUIDV4
        },
        name: {
            type: Sequelize.TEXT
        },
        serial_key: {
            type: Sequelize.TEXT
        },
        sum: {
            type: Sequelize.FLOAT,
            allowNull: false,
            defaultValue: 0
        }
    }, {
        sequelize,
        modelName: 'payRow',
        schema: 'licenses',
        underscored: true
    });

    return PayRow;
};
