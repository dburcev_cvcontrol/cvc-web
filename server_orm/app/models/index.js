const dbConfig = require('../config/env').database;

const Sequelize = require('sequelize');

const sequelize = new Sequelize(dbConfig.database, dbConfig.user, dbConfig.password, {
    host: dbConfig.host,
    dialect: dbConfig.dialect,
    logging: false,
    pool: dbConfig.pool
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//Создаем схему и в ней уже создаем модели
//Пользователи
db.users  = require('./user.model')(db.sequelize, db.Sequelize);
//Права
db.rights = require('./right.model')(db.sequelize, db.Sequelize);
//Роли
db.roles  = require('./role.model')(db.sequelize, db.Sequelize);

//Связь ролей и прав
db.rights.belongsToMany(db.roles, {through: 'role_rights'});
db.roles.belongsToMany(db.rights, {through: 'role_rights'});

//Связь пользователей и ролей
db.roles.belongsToMany(db.users, {through: 'user_roles'});
db.users.belongsToMany(db.roles, {through: 'user_roles'});

//Группы клиентов
db.clientGroups  = require('./clientGroup.model')(db.sequelize, db.Sequelize);
//Клиенты
db.clients  = require('./client.model')(db.sequelize, db.Sequelize);
//Плагины
db.plugins  = require('./plugin.model')(db.sequelize, db.Sequelize);
//Лицензии
db.licenses  = require('./license.model')(db.sequelize, db.Sequelize);
//Документы оплаты
db.pays  = require('./pay.model')(db.sequelize, db.Sequelize);
//Строки документов оплаты
db.payRows  = require('./payRow.model')(db.sequelize, db.Sequelize);

//Связь группы и клиентов
db.clientGroups.hasMany(db.clients, {foreignKey: 'id_group'});
db.clients.belongsTo(db.clientGroups, {foreignKey: 'id_group'});

//Связь клиента и лицензии
db.clients.hasMany(db.licenses, {foreignKey: 'id_client'});
db.licenses.belongsTo(db.clients, {foreignKey: 'id_client'});

//Связь лицензий и плагинов
db.licenses.belongsToMany(db.plugins, {through: 'license_plugins'});
db.plugins.belongsToMany(db.licenses, {through: 'license_plugins'});

//Связь клиентов и документов оплаты
db.clients.hasMany(db.pays, {foreignKey: 'id_client'});
db.pays.belongsTo(db.clients, {foreignKey: 'id_client'});

//Связь Документов оплаты и строк
db.pays.hasMany(db.payRows, {foreignKey: 'id_pay'});
db.payRows.belongsTo(db.pays, {foreignKey: 'id_pay'});

module.exports = db;
