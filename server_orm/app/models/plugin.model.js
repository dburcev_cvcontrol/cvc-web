module.exports = (sequelize, Sequelize) => {
    class Plugin extends Sequelize.Model {}

    Plugin.init({
        id_plugin: {
            type: Sequelize.UUID,
            allowNull: false,
            primaryKey: true,
            defaultValue: Sequelize.UUIDV4
        },
        sysname: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        name: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        comment: {
            type: Sequelize.TEXT
        }
    }, {
        sequelize,
        modelName: 'plugin',
        schema: 'licenses',
        underscored: true
    });

    return Plugin;
};
