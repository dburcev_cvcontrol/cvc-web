module.exports = (sequelize, Sequelize) => {
    const Role = sequelize.define('role', {
        id_role: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.TEXT,
            allowNull: false,
            unique: true
        }
    }, {
        schema: 'core',
        underscored: true
    });

    return Role;
};
