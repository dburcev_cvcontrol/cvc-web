module.exports = (sequelize, Sequelize) => {
    class Pay extends Sequelize.Model {}

    Pay.init({
        id_pay: {
            type: Sequelize.UUID,
            allowNull: false,
            primaryKey: true,
            defaultValue: Sequelize.UUIDV4
        },
        plan_number: {
            type: Sequelize.TEXT
        },
        plan_date: {
            type: Sequelize.INTEGER
        },
        fact_number: {
            type: Sequelize.TEXT
        },
        fact_date: {
            type: Sequelize.INTEGER
        },
        comment: {
            type: Sequelize.TEXT
        },
        sum: {
            type: Sequelize.FLOAT,
            allowNull: false,
            defaultValue: 0
        }
    }, {
        sequelize,
        modelName: 'pay',
        schema: 'licenses',
        underscored: true
    });

    return Pay;
};
