module.exports = (sequelize, Sequelize) => {
    const Right = sequelize.define('right', {
        id_right: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        sysname: {
            type: Sequelize.TEXT,
            allowNull: false,
            unique: true
        },
        name: {
            type: Sequelize.TEXT,
            allowNull: false
        }
    }, {
        schema: 'core',
        underscored: true
    });

    return Right;
};
