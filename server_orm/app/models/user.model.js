module.exports = (sequelize, Sequelize) => {
    class User extends Sequelize.Model {}

    User.init({
        id_user: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        login: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        password: {
            type: Sequelize.TEXT
        },
        name: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        comment: {
            type: Sequelize.TEXT
        },
        active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        superuser: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        getterMethods: {
          async rights() {
              let resRights = [];
              let promises = [];
              let resRoles = await this.getRoles();

              resRoles.forEach(role => {
                  promises.push(role.getRights().then( rights => {
                      rights.forEach(right => {
                          if (resRights.indexOf(right.get('sysname')) === -1) {
                              resRights.push(right.get('sysname'));
                          }
                      });
                  }));
              });

              await Promise.all(promises).then(() => {});

              return resRights;
          }
        },
        sequelize,
        modelName: 'user',
        schema: 'core',
        underscored: true
    });

    return User;
};
