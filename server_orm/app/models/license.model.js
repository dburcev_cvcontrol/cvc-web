module.exports = (sequelize, Sequelize) => {
    class License extends Sequelize.Model {}

    License.init({
        id_license: {
            type: Sequelize.UUID,
            allowNull: false,
            primaryKey: true,
            defaultValue: Sequelize.UUIDV4
        },
        serial_key: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        date_finish: {
            type: Sequelize.INTEGER
        },
        active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        sort_order: {
            type: Sequelize.INTEGER,
            allowNull: false,
            autoIncrement: true
        }
    }, {
        sequelize,
        modelName: 'license',
        schema: 'licenses',
        underscored: true,
        paranoid: true
    });

    return License;
};
