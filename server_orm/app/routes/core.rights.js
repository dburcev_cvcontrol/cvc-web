//Роуты обработки прав

//Подключаем основной фреймфорк
const express = require('express');

//Подключаем контроллеры для событий
const controller = require('../controllers/core.rights');
const router = express.Router();

//Описываем роуты
router.get('/', controller.getRights);
router.get('/detail/:id', controller.getRight);
router.post('/', controller.addRight);
router.put('/', controller.updateRight);
router.delete('/:id', controller.removeRight);

module.exports = router;
