//Роуты обработки ролей

//Подключаем основной фреймфорк
const express = require('express');

//Подключаем контроллеры для событий
const controller = require('../controllers/core.roles');
const router = express.Router();

//Описываем роуты
router.get('/', controller.getRoles);
router.get('/detail/:id', controller.getRole);
router.post('/', controller.addRole);
router.put('/', controller.updateRole);
router.delete('/:id', controller.removeRole);

module.exports = router;
