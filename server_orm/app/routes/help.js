//Роуты обработки событий

//Подключаем основной фреймфорк
const express = require('express');
const path = require('path');

//Подключаем контроллеры для событий
const router = express.Router();

//Описываем роуты
router.get('/', (request, response) => {
    response.sendFile(path.resolve(__dirname, '..', 'views','index.html'));
});

router.get('/auth.html', (request, response) => {
    response.sendFile(path.resolve(__dirname, '..', 'views','auth.html'));
});

router.get('/rights.html', (request, response) => {
    response.sendFile(path.resolve(__dirname, '..', 'views','rights.html'));
});

router.get('/roles.html', (request, response) => {
    response.sendFile(path.resolve(__dirname, '..', 'views','roles.html'));
});

router.get('/users.html', (request, response) => {
    response.sendFile(path.resolve(__dirname, '..', 'views','users.html'));
});

router.get('/clients.html', (request, response) => {
    response.sendFile(path.resolve(__dirname, '..', 'views','clients.html'));
});

router.get('/groups.html', (request, response) => {
    response.sendFile(path.resolve(__dirname, '..', 'views','groups.html'));
});

router.get('/plugins.html', (request, response) => {
    response.sendFile(path.resolve(__dirname, '..', 'views','plugins.html'));
});

router.get('/licenses.html', (request, response) => {
    response.sendFile(path.resolve(__dirname, '..', 'views','licenses.html'));
});

router.get('/interfaces.html', (request, response) => {
    response.sendFile(path.resolve(__dirname, '..', 'views','interfaces.html'));
});

module.exports = router;
