//Роуты обработки пользователей

//Подключаем основной фреймфорк
const express = require('express');

//Подключаем контроллеры для событий
const controller = require('../controllers/core.users');
const router = express.Router();

//Описываем роуты
router.get('/', controller.getUsers);
router.get('/detail/:id', controller.getUser);
router.post('/', controller.addUser);
router.post('/resetpassword', controller.resetPassword);
router.put('/', controller.updateUser);
router.delete('/:id', controller.removeUser);

module.exports = router;
